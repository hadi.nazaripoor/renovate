﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Variables
{
    public class RuntimeVariables
    {
        public static string ApplicationPath { get; set; }
        public static string AppExeFileName { get; set; }
        public static string AppExeFullFileName { get; set; }

        public RuntimeVariables()
        {
        }
    }
}
