﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("CostRevenueCats")]
    [Model(SingleName = "دسته هزینه و درآمد", MultipleName = "دسته های هزینه و درآمد")]
    public class CostRevenueCat : BaseModel
    {
        private short costRevenueCatId;
        private short? parentCostRevenueCatId;
        private string title;
        private string comment = "";

        [DataMember]
        [Key]
        public short CostRevenueCatId
        {
            get { return costRevenueCatId; }
            set { this.RaiseAndSetIfChanged(ref costRevenueCatId, value); }
        }

        [DataMember]
        public short? ParentCostRevenueCatId
        {
            get { return parentCostRevenueCatId; }
            set { this.RaiseAndSetIfChanged(ref parentCostRevenueCatId, value); }
        }

        [DataMember]
        [Required]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged(ref title, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        [NotMapped]
        public List<CostRevenueCat> Children { get; set; } = new List<CostRevenueCat>();

        public CostRevenueCat()
        {
            this.ValidationRule(vm => vm.Title, title => !title.IsNullOrWhiteSpace(), "عنوان نمی تواند خالی باشد");
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
