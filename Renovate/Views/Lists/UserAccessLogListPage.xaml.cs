﻿using ReactiveUI;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using System;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for UserAccessLogListPage.xaml
    /// </summary>
    public partial class UserAccessLogListPage : BaseListPage<UserAccessLog>
    {
        public UserAccessLogListPage()
        {
            InitializeComponent();
            var searchModel = new UserAccessLogSearchModel();

            IObservable<Func<UserAccessLog, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Title)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Title = s }).Select(s => new Func<UserAccessLog, bool>(p => true));

            Initialize(null, new UserAccessLogRepository(), searchModel, dynamicFilter);
        }
    }
}
