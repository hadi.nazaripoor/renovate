﻿using Renovate.Assets.DTOs;
using Renovate.Models;
using Renovate.Repositories;
using Renovate.ViewModels;
using Renovate.ViewModels.ListViewModels;
using Renovate.Views.CUs;
using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace Renovate.Views.Lists
{
    public class BaseListPage<T> : Page where T : BaseModel, new()
    {
        public BaseListViewModel<T> ViewModel { get; private set; }
        KeyEventHandler WindowPreviewKeyDownEventHandler;

        public BaseListPage()
        {
            var window = App.Current.MainWindow as MainWindow;

            WindowPreviewKeyDownEventHandler = (s, e) => { ListBasePage_PreviewKeyDown(s, e); };
            this.Loaded += (s, e) => { window.PreviewKeyDown += WindowPreviewKeyDownEventHandler; };
            this.Unloaded += (s, e) => { window.PreviewKeyDown -= WindowPreviewKeyDownEventHandler; };
        }

        public void Initialize(BaseCuPage<T> createUpdatePage, BaseRepository<T> repository, SearchBaseDto searchModel, IObservable<Func<T, bool>> criteria)
        {
            ViewModel = new BaseListViewModel<T>(repository, searchModel, criteria)
            {
                CreateUpdatePage = createUpdatePage
            };
            this.Unloaded += (s, e) => { ViewModel.DisposeCommand.Execute().Subscribe(); };
            this.DataContext = ViewModel;
        }

        private void ListBasePage_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control || e.Key == Key.Delete || e.Key == Key.Escape)
            {
                switch (e.Key)
                {
                    case Key.N:
                        ViewModel.CreateCommand.Execute().Subscribe();
                        break;

                    case Key.Delete:
                        ViewModel.RemoveCommand.Execute().Subscribe();
                        break;

                    case Key.Escape:
                        //listBasePage.Close(null, null);
                        break;

                    case Key.P:
                        //ViewModel.PrintCommand.Execute().Subscribe();
                        break;

                    case Key.A:
                        ViewModel.SelectAllCommand.Execute().Subscribe();
                        break;

                    case Key.F:
                        ViewModel.SearchCommand.Execute().Subscribe();
                        break;

                    default:
                        return; // RETURN => e.Handled = false; => Propagate KeyDown Event
                }

                e.Handled = true;
            }
        }
    }
}
