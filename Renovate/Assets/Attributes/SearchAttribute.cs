﻿using System;

namespace Renovate.Assets.Attributes
{
    public class SearchAttribute : Attribute
    {
        public string Title { get; set; }
    }
}
