﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for PersonListPage.xaml
    /// </summary>
    public partial class PersonListPage : BaseListPage<Person>
    {
        public PersonListPage()
        {
            InitializeComponent();
            var searchModel = new PersonSearchModel();

            IObservable<Func<Person, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.FullName, s => s.CallNumbers, s => s.NationalCodeOrEconomicCode)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { FullName = s.Item1, CallNumbers = s.Item2, NationalCode = s.Item3 })
                .Select(s => new Func<Person, bool>(p => 
                    (s.FullName.IsNullOrWhiteSpace() || p.FullName.Contains(s.FullName)) &&
                    (s.CallNumbers.IsNullOrWhiteSpace() || p.CallNumbers.Contains(s.CallNumbers)) && 
                    (s.NationalCode.IsNullOrWhiteSpace() || p.NationalCodeOrEconomicCode.Contains(s.NationalCode))));

            Initialize(new PersonPage(), new PersonRepository(), searchModel, dynamicFilter);
        }
    }
}
