﻿using ReactiveUI;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for PersonTransactionListPage.xaml
    /// </summary>
    public partial class PersonTransactionListPage : BaseListPage<PersonTransaction>
    {
        public PersonTransactionListPage()
        {
            InitializeComponent();
            var searchModel = new PersonTransactionSearchModel();

            var dynamicFilter = searchModel.WhenAnyValue(s => s.Title)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Title = s }).Select(s => new Func<PersonTransaction, bool>(p => true));

            Initialize(new PersonTransactionPage(), new PersonTransactionRepository(), searchModel, dynamicFilter);
        }
    }
}
