﻿using Renovate.Assets.DTOs;
using Renovate.Assets.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renovate.Assets.UI.UserControls
{
    /// <summary>
    /// Interaction logic for EnumMultiSelectUserControl.xaml
    /// </summary>
    public partial class EnumMultiSelectUserControl : UserControl
    {
        public EnumMultiSelectUserControl()
        {
            InitializeComponent();
        }

        public string Title
        {
            get { return (string)base.GetValue(TitleProperty); }
            set { base.SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(EnumMultiSelectUserControl), new PropertyMetadata(""));

        public int Value
        {
            get { return (int)base.GetValue(ValueProperty); }
            set { base.SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(int), typeof(EnumMultiSelectUserControl), new FrameworkPropertyMetadata(0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DataChanged));

        public Type EnumType
        {
            get { return (Type)base.GetValue(EnumTypeProperty); }
            set { base.SetValue(EnumTypeProperty, value); }
        }
        public static readonly DependencyProperty EnumTypeProperty = DependencyProperty.Register("EnumType", typeof(Type), typeof(EnumMultiSelectUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DataChanged));

        private static void DataChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (EnumMultiSelectUserControl)dependencyObject;

            var sourceEnumType = userControl.EnumType;
            var value = userControl.Value;
            var enumValues = userControl.EnumValues;

            if (sourceEnumType != null)
            {
                if (enumValues.Count == 0)
                {
                    var enumItems = sourceEnumType.ToCollection();
                    foreach (var enumItem in enumItems)
                        enumValues.Add(enumItem);
                }

                foreach (var enumValue in enumValues)
                    enumValue.IsSelected = value != 0 && (value | (int)enumValue.Value) == value;
            }
        }

        public ObservableCollection<EnumDto> EnumValues { get; set; } = new ObservableCollection<EnumDto>();

        private void UserControl_LostFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = false;
        }

        private void UserControl_GotFocus(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.StaysOpen = true;
        }

        private void SelectDeselectButton_Click(object sender, RoutedEventArgs e)
        {
            var item = (sender as Button).DataContext as EnumDto;
            Value ^= (int)item.Value;

            //item.IsSelected = !item.IsSelected;
        }

        private void ToggleVisibility(object sender, RoutedEventArgs e)
        {
            SuggestionGrid.IsOpen = !SuggestionGrid.IsOpen;
        }
    }
}
