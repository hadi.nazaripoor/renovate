﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.DTOs
{
    public class CalendarDateTimeDto : DateTimeDto
    {
        public DateTime ValidDate { get; set; }

        private string stringDate;
        public string StringDate
        {
            get { return stringDate; }
            set { this.RaiseAndSetIfChanged(ref stringDate , value); }
        }

        private bool isInYearMode;
        public bool IsInYearMode
        {
            get { return isInYearMode; }
            set { this.RaiseAndSetIfChanged(ref isInYearMode, value); }
        }

        private string calendarDisplayDate;
        public string CalendarDisplayDate
        {
            get { return calendarDisplayDate; }
            set { this.RaiseAndSetIfChanged(ref calendarDisplayDate, value); }
        }

        private bool isLocked = false;
        public CalendarDateTimeDto()
        {
            PropertyChanged += (s, e) =>
            {
                if (isLocked)
                    return;

                isLocked = true;
                var forValidationProperties = new string[] { nameof(Year), nameof(Month), nameof(Day), nameof(StringDate) };
                if (forValidationProperties.Contains(e.PropertyName))
                {
                    var stringToConvert = this.ToString();
                    if (e.PropertyName == nameof(StringDate))
                        stringToConvert = StringDate;

                    if (stringToConvert.TryConvertToDate(out DateTime forValidate))
                    {
                        ValidDate = forValidate;
                        if (e.PropertyName != nameof(StringDate))
                            StringDate = this.ToString();
                        else
                        {
                            string[] dateParts = StringDate.Split('/');

                            Year = int.Parse(dateParts[0]);
                            Month = int.Parse(dateParts[1]);
                            Day = int.Parse(dateParts[2]);
                        }
                    }
                }
                isLocked = false;
            };
        }
    }
}
