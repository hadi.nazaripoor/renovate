﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Enums
{
    [Flags]
    public enum PersonTypes
    {
        [Display(Name = "مشتری")]
        Customer = 1,
        [Display(Name = "فروشنده")]
        Company = 2,
        [Display(Name = "پرسنل")]
        Personnel = 4,
        [Display(Name = "کاربر نرم افزار")]
        User = 8,
        [Display(Name = "بازاریاب")]
        Marketer = 16,
        [Display(Name = "مالک")]
        Owner = 32,
        [Display(Name = "سرمایه گذار")]
        Investor = 64
    }
}
