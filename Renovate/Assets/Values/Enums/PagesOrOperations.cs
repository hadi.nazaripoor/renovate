﻿using Renovate.Assets.Attributes;
using System;

namespace Renovate.Assets.Values.Enums
{
    public enum PagesOrOperations
    {
        [NavigationLink(Title = "", Icon = "")]
        None = 0,

        [NavigationLink(Title = "قالب تراکنش شخص", Icon = "")]
        PersonTransactionTemplate,
        [NavigationLink(Title = "لیست دفتر چه های چک", Icon = "")]
        ChequeBookList,
        [NavigationLink(Title = "ورود اشخاص از اکسل", Icon = "")]
        PersonFromExcel,
        [NavigationLink(Title = "قالب فاکتور", Icon = "")]
        FactorTemplate,

        [NavigationLink(Title = "فروش جدید", Icon = "")]
        Sale,
        [NavigationLink(Title = "لیست فاکتورهای فروش", Icon = "")]
        SaleList,
        [NavigationLink(Title = "پیش فاکتور فروش  فروش جدید", Icon = "")]
        SalePreFactor,
        [NavigationLink(Title = "لیست پیش فاکتورهای فروش", Icon = "")]
        SalePreFactorList,

        [NavigationLink(Title = "خرید جدید", Icon = "")]
        Buy,
        [NavigationLink(Title = "لیست فاکتورهای خرید", Icon = "")]
        BuyList,

        [NavigationLink(Title = "دریافت بدهی جدید", Icon = "")]
        Recieve,
        [NavigationLink(Title = "پرداخت طلب جدید", Icon = "")]
        Payment,
        [NavigationLink(Title = "لیست انتقال حساب ها", Icon = "", NavigationPageSource = "Views/Lists/AccountTransitionListPage.xaml")]
        AccountTransitionList,
        [NavigationLink(Title = "انتقال وجه بین حساب های جدید", Icon = "")]
        AccountTransition,
        [NavigationLink(Title = "لیست تراکنش های مشتریان", Icon = "", NavigationPageSource = "Views/Lists/PersonTransactionListPage.xaml")]
        PersonTransactionListPage,
        [NavigationLink(Title = "چک دریافتنی جدید", Icon = "")]
        RecieveableCheque,
        [NavigationLink(Title = "لیست چک های دریافتنی", Icon = "")]
        RecieveableChequeList,
        [NavigationLink(Title = "چک پرداختنی جدید", Icon = "")]
        PayableCheque,
        [NavigationLink(Title = "لیست چک های پرداختنی", Icon = "")]
        PayableChequeList,
        [NavigationLink(Title = "تقسیط جدید", Icon = "")]
        Loan,
        [NavigationLink(Title = "لیست دفترچه های قسط", Icon = "")]
        LoanList,

        [NavigationLink(Title = "هزینه جدید", Icon = "")]
        Outgo,
        [NavigationLink(Title = "لیست هزینه ها", Icon = "")]
        OutgoList,
        [NavigationLink(Title = "درآمد جدید", Icon = "")]
        Income,
        [NavigationLink(Title = "لیست درآمدها", Icon = "")]
        IncomeList,
        [NavigationLink(Title = "لیست دسته های درآمد و هزینه", Icon = "", NavigationPageSource = "Views/Lists/CostRevenueCatListPage.xaml")]
        CostRevenueCatList,
        [NavigationLink(Title = "لیست برچسب ها", Icon = "")]
        TagList,
        [NavigationLink(Title = "لیست حساب ها", Icon = "", NavigationPageSource = "Views/Lists/AccountListPage.xaml")]
        AccountList,
        [NavigationLink(Title = "شخص جدید", Icon = "")]
        Person,
        [NavigationLink(Title = "لیست اشخاص", Icon = "", NavigationPageSource = "Views/Lists/PersonListPage.xaml")]
        PersonList,
        [NavigationLink(Title = "لیست گروه های اشخاص", Icon = "")]
        PersonGroupList,

        [NavigationLink(Title = "لیست محصولات", Icon = "", NavigationPageSource = "Views/Lists/ProductListPage.xaml")]
        ProductList,
        [NavigationLink(Title = "محصول جدید", Icon = "")]
        Product,
        [NavigationLink(Title = "ورود محصولات از اکسل", Icon = "")]
        ProductFromExcel,
        [NavigationLink(Title = "لیست دسته های محصولات", Icon = "", NavigationPageSource = "Views/Lists/ProductCatListPage.xaml")]
        ProductCatList,
        [NavigationLink(Title = "لیست واحدهای اندازه گیری", Icon = "", NavigationPageSource = "Views/Lists/CountUnitListPage.xaml")]
        UnitList,
        [NavigationLink(Title = "محصولات مورد نیاز", Icon = "")]
        NeededProduct,

        [NavigationLink(Title = "لیست پیامک ها", Icon = "")]
        SMSList,
        [NavigationLink(Title = "پیامک جدید", Icon = "")]
        SMS,
        [NavigationLink(Title = "لیست پیام های تلگرامی", Icon = "")]
        TelegramList,
        [NavigationLink(Title = "پیام تلگرامی جدید", Icon = "")]
        Telegram,
        [NavigationLink(Title = "لیست پیام های دریافتی از تلگرام", Icon = "")]
        RecievedTelegramList,

        [NavigationLink(Title = "داشبورد", Icon = "")]
        Dashboard,
        [NavigationLink(Title = "لیست گزارشات", Icon = "")]
        ReportList,
        [NavigationLink(Title = "گزارش تراکنش های حساب", Icon = "")]
        AccountTransactionReport,
        [NavigationLink(Title = "گزارش تراکنش های کاربر", Icon = "")]
        PersonTransactionReport,
        [NavigationLink(Title = "گزارش مالیات", Icon = "")]
        TaxReport,
        [NavigationLink(Title = "گزارش کاردکس محصول", Icon = "")]
        ProductCartexReport,
        [NavigationLink(Title = "گزارش ارزش محصول", Icon = "")]
        ProductValueReport,
        [NavigationLink(Title = "گزارش هزینه و درآمد", Icon = "")]
        OutgoIncomeReport,
        [NavigationLink(Title = "گزارش اقساط", Icon = "")]
        LoanReport,
        [NavigationLink(Title = "گزارش چک ها", Icon = "")]
        ChequeReport,
        [NavigationLink(Title = "گزارش خرید و فروش", Icon = "")]
        BuySaleReport,
        [NavigationLink(Title = "گزارش کلی", Icon = "")]
        OverallReport,
        [NavigationLink(Title = "گزارش مالی", Icon = "")]
        FinanceTReport,
        [NavigationLink(Title = "گزارش سود و زیان", Icon = "")]
        GainLoseReport,

        [NavigationLink(Title = "لیست کاربران", Icon = "", NavigationPageSource = "Views/Lists/UserListPage.xaml")]
        UserList,
        [NavigationLink(Title = "کاربر جدید", Icon = "")]
        User,
        [NavigationLink(Title = "دسترسی های کاربر", Icon = "")]
        UserPermission,
        [NavigationLink(Title = "تغییر کلمه عبور", Icon = "")]
        ChangePassword,
        [NavigationLink(Title = "تاریخچه ورود و خروج کاربران", Icon = "")]
        UserAccessLogList,

        [NavigationLink(Title = "لیست یادداشت ها", Icon = "", NavigationPageSource = "Views/Lists/NoteListPage.xaml")]
        NoteList,
        [NavigationLink(Title = "تنظیمات نرم افزار", Icon = "")]
        Settings,
        [NavigationLink(Title = "لیست کلیدهای میانبر", Icon = "")]
        ShortcutList,
        [NavigationLink(Title = "پشتیبان گیری از داده ها", Icon = "", NavigationPageSource = "Views/Lists/ShortcutListPage.xaml")]
        Backup_Data,
        [NavigationLink(Title = "بازیابی داده ها", Icon = "")]
        Restore_Data,
        [NavigationLink(Title = "فعالسازی نرم افزار", Icon = "")]
        ProductActivation,

        [NavigationLink(Title = "راهنمای زنده", Icon = "")]
        LiveHelp,
        [NavigationLink(Title = "راهنمای ویدیویی", Icon = "")]
        VideoHelp,
        [NavigationLink(Title = "راهنمای متنی", Icon = "")]
        TextHelp,
        [NavigationLink(Title = "درباره نرم افزار", Icon = "")]
        AboutSoftware,
        [NavigationLink(Title = "درباره ما", Icon = "")]
        AboutUs
    }
}
