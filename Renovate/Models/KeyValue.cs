﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.Values.Constants;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("KeyValues")]
    [Model(SingleName = "کلید مقدار", MultipleName = "کلید مقدار ها")]
    public class KeyValue : BaseModel
    {
        private string key;
        private string _value = "";

        [DataMember]
        [Key]
        [MaxLength(LengthConstants.LARGE_STRING)]
        public string Key
        {
            get { return key; }
            set { this.RaiseAndSetIfChanged(ref key, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.LARGE_STRING)]
        public string Value
        {
            get { return _value; }
            set { this.RaiseAndSetIfChanged(ref _value, value); }
        }

        public KeyValue()
        {
        }
    }
}
