﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
    public class AccountSearchModel : SearchBaseDto
    {
		private string accountTitle;

		[Search(Title = "عنوان حساب")]
		public string AccountTitle
		{
			get { return accountTitle; }
			set { this.RaiseAndSetIfChanged(ref accountTitle, value); }
		}
	}
}
