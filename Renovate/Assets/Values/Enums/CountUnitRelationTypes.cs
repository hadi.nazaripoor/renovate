﻿using System.ComponentModel.DataAnnotations;

namespace Renovate.Assets.Values.Enums
{
    public enum CountUnitRelationTypes
    {
        [Display(Name = "تک واحدی")]
        None = 0,
        [Display(Name = "دو واحده، کل و جزء")]
        Partial = 1,
        [Display(Name = "دو واحده وابسته")]
        Completive = 2
    }
}
