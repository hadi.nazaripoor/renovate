﻿using Renovate.Assets.Values.Enums;

namespace Renovate.Assets.DTOs
{
    public class OperationResultDto
    {
        public string Message { get; set; }
        public OperationResults Result { get; set; } = OperationResults.Succeeded;
    }
}
