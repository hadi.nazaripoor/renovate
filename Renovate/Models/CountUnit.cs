﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("CountUnits")]
    [Model(SingleName = "واحد شمارش", MultipleName = "واحدهای اندازه گیری")]
    public class CountUnit : BaseModel
    {
        private short countUnitId;
        private CountUnitRelationTypes relationType;
        private string firstUnitTitle;
        private string secondUnitTitle = "";
        private int optionalWholeCapacity = 1;
        private string stringFormat = "";
        private string comment = "";

        [DataMember]
        [Key]
        public short CountUnitId
        {
            get { return countUnitId; }
            set { this.RaiseAndSetIfChanged(ref countUnitId, value); }
        }

        [DataMember]
        [Required]
        public CountUnitRelationTypes RelationType
        {
            get { return relationType; }
            set { this.RaiseAndSetIfChanged(ref relationType, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "عنوان واحد اول نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string FirstUnitTitle
        {
            get { return firstUnitTitle; }
            set { this.RaiseAndSetIfChanged(ref firstUnitTitle, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string SecondUnitTitle
        {
            get { return secondUnitTitle; }
            set { this.RaiseAndSetIfChanged(ref secondUnitTitle, value); }
        }

        [DataMember]
        [Required]
        public int OptionalWholeCapacity
        {
            get { return optionalWholeCapacity; }
            set { this.RaiseAndSetIfChanged(ref optionalWholeCapacity, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string StringFormat
        {
            get { return stringFormat; }
            set { this.RaiseAndSetIfChanged(ref stringFormat, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        public CountUnit()
        {
            this.ValidationRule(vm => vm.FirstUnitTitle, firstUnitTitle => !firstUnitTitle.IsNullOrWhiteSpace(), "عنوان واحد اول نمی تواند خالی باشد");
        }

        public override string ToString()
        {
            return FirstUnitTitle;
        }
    }
}
