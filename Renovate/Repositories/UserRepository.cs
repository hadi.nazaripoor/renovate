﻿using Renovate.Assets.Extensions;
using Renovate.Assets.Utilities;
using Renovate.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Renovate.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public override bool Insert(User modelToInsert)
        {
            var modelKeyValues = modelToInsert?.GetKeyValuePairs();
            var parameters = modelKeyValues.Select(keyValue => new SqlParameter(keyValue.Key, keyValue.Value)).ToList();
            parameters.Where(p => p.ParameterName == nameof(User.AvatarImageBytes)).Select(ip => { ip.Value = Encoding.ASCII.GetBytes(ip.Value.ToString()); return ip; }).ToList();

            return SqlUtil.ExecuteNonQuery($"Insert Into {typeof(User).GetAttribute<TableAttribute>().Name} ({modelKeyValues.Select(m => m.Key).Join(", ")}) Values ({modelKeyValues.Select(m => $"@{m.Key}").Join(", ")})", commandParameters: parameters) > 0;
        }
    }
}
