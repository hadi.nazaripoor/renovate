﻿namespace Renovate.Assets.Values.Enums
{
    public enum DialogResults
    {
        None,
        OK,
        Cancel,
        Yes,
        No
    }
}
