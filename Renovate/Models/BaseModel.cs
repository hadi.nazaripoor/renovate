﻿using ReactiveUI;
using Renovate.Assets.DTOs;

namespace Renovate.Models
{
    public class BaseModel : BaseDto
    {
        private long order;
        public long Order
        {
            get { return order; }
            set { this.RaiseAndSetIfChanged(ref order, value); }
        }

        private bool isVisible;
        public bool IsVisible
        {
            get { return isVisible; }
            set { this.RaiseAndSetIfChanged(ref isVisible, value); }
        }

        private bool isTotalRow;
        public bool IsTotalRow
        {
            get { return isTotalRow; }
            set { this.RaiseAndSetIfChanged(ref isTotalRow, value); }
        }

        public BaseModel()
        {
        }
    }
}
