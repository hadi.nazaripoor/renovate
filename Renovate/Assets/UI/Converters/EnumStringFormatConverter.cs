﻿using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;
using Renovate.Assets.Extensions;
using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Renovate.Assets.UI.Converters
{
    class EnumStringFormatConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var enumItems = values[0] as ObservableCollection<EnumDto>;
            if (values != null)
            {
                var selectedCount = enumItems.Count(item => item.IsSelected);

                var enumType = (Type)values[1];
                if (enumType != null)
                {
                    var enumAttribute = enumType.GetAttribute<EnumAttribute>();
                    if (selectedCount == 0)
                        return enumAttribute.NonSelectedStringFormat;
                    else if (selectedCount == 1)
                        return String.Format(enumAttribute.SingleSelectedStringFormat, enumItems.FirstOrDefault(item => item.IsSelected).Description);
                    else
                        return String.Format(enumAttribute.MultipleSelectedStringFormat, selectedCount);
                }
            }

            return "";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
