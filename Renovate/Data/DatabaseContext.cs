﻿using Renovate.Assets.Values.Variables;
using Renovate.Models;
using System.Data.Entity;

namespace Renovate.Data
{
    class DatabaseContext : DbContext
    {
        public DatabaseContext() : base(DatabaseVariables.ConnectionString)
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountTransition> AccountTransitions { get; set; }
        public DbSet<CostRevenueCat> CostRevenueCats { get; set; }
        public DbSet<CountUnit> CountUnits { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<ProductCat> ProductCats { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<PersonTransaction> PersonTransactions { get; set; }
        public DbSet<Shortcut> Shortcuts { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountTransition>()
                                .HasRequired(accountTransition => accountTransition.SourceAccount)
                                .WithMany()
                                .HasForeignKey(account => account.SourceAccountId)
                                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AccountTransition>()
                                .HasRequired(accountTransition => accountTransition.DestinationAccount)
                                .WithMany()
                                .HasForeignKey(account => account.DestinationAccountId)
                                .WillCascadeOnDelete(false);
            //
        }
    }
}
