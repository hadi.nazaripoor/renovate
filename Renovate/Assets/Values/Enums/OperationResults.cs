﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Enums
{
    public enum OperationResults
    {
        Succeeded,
        Failed,
        Warned
    }
}
