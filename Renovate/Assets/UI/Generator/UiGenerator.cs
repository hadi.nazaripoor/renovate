﻿using Renovate.Assets.UI.BaseClasses;

namespace Renovate.Assets.UI.Generator
{
    public class UiGenerator
    {
        public static JTextBox GetTextBox(string title)
        {
            return new JTextBox { Title = title, Height = 42 };
        }
    }
}
