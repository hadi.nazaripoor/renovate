﻿namespace Renovate.Assets.DTOs
{
	public class SearchBaseDto : BaseDto
    {
		public string Title { get; set; }
	}
}
