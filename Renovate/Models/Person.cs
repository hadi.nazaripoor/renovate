﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("People")]
    [Model(SingleName = "شخص", MultipleName = "اشخاص")]
    public class Person : BaseModel
    {
        private int personId;
        private string firstName;
        private string lastName;
        private string telephone = "";
        private string mobile = "";
        private string nationalCodeOrEconomicCode = "";
        private string bankAccountNo = "";
        private string bankAccountCardNo = "";
        private string bankAccountId = ""; // شماره شبا
        private string email = "";
        private string address = "";

        private long primaryBalance;
        private long currentBalance;
        private long maxValidDebit;

        private PersonTypes personTypes;

        private bool isDeleted;

        [DataMember]
        [Key]
        public int PersonId
        {
            get { return personId; }
            set { this.RaiseAndSetIfChanged(ref personId, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "نام نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string FirstName
        {
            get { return firstName; }
            set { this.RaiseAndSetIfChanged(ref firstName, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "نام خانوادگی نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string LastName
        {
            get { return lastName; }
            set { this.RaiseAndSetIfChanged(ref lastName, value); }
        }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.CALL_NUMBERS)]
        public string Telephone
        {
            get { return telephone; }
            set { this.RaiseAndSetIfChanged(ref telephone, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.CALL_NUMBERS)]
        public string Mobile
        {
            get { return mobile; }
            set { this.RaiseAndSetIfChanged(ref mobile, value); }
        }

        [NotMapped]
        public string CallNumbers
        {
            get
            {
                var result = "";
                if (Telephone != "") result = Telephone;
                if (Mobile != "") result += result == "" ? Mobile : ", " + Mobile;

                return result;
            }
        }

        [DataMember]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string NationalCodeOrEconomicCode
        {
            get { return nationalCodeOrEconomicCode; }
            set { this.RaiseAndSetIfChanged(ref nationalCodeOrEconomicCode, value); }
        }

        [DataMember]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string BankAccountNo
        {
            get { return bankAccountNo; }
            set { this.RaiseAndSetIfChanged(ref bankAccountNo, value); }
        }

        [DataMember]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string BankAccountCardNo
        {
            get { return bankAccountCardNo; }
            set { this.RaiseAndSetIfChanged(ref bankAccountCardNo, value); }
        }

        [DataMember]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string BankAccountId
        {
            get { return bankAccountId; }
            set { this.RaiseAndSetIfChanged(ref bankAccountId, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.LARGE_STRING)]
        public string Email
        {
            get { return email; }
            set { this.RaiseAndSetIfChanged(ref email, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.LARGE_STRING)]
        public string Address
        {
            get { return address; }
            set { this.RaiseAndSetIfChanged(ref address, value); }
        }

        [DataMember]
        [Required]
        public long PrimaryBalance
        {
            get { return primaryBalance; }
            set { this.RaiseAndSetIfChanged(ref primaryBalance, value); }
        }

        [DataMember]
        [Required]
        public long CurrentBalance
        {
            get { return currentBalance; }
            set { this.RaiseAndSetIfChanged(ref currentBalance, value); }
        }

        [DataMember]
        [Required]
        public long MaxValidDebit
        {
            get { return maxValidDebit; }
            set { this.RaiseAndSetIfChanged(ref maxValidDebit, value); }
        }

        [DataMember]
        [Required]
        public bool IsDeleted
        {
            get { return isDeleted; }
            set { this.RaiseAndSetIfChanged(ref isDeleted, value); }
        }

        public Person()
        {
            this.ValidationRule(vm => vm.FirstName, fName => !fName.IsNullOrWhiteSpace(), "نام نمی تواند خالی باشد");
            this.ValidationRule(vm => vm.LastName, lName => !lName.IsNullOrWhiteSpace(), "نام خانوادگی نمی تواند خالی باشد");
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}";
        }
    }
}
