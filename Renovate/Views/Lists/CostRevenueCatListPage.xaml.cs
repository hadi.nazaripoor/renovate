﻿using ReactiveUI;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for CostRevenueCatListPage.xaml
    /// </summary>
    public partial class CostRevenueCatListPage : BaseListPage<CostRevenueCat>
    {
        public CostRevenueCatListPage()
        {
            InitializeComponent();
            var searchModel = new CostRevenueCatSearchModel();

            IObservable<Func<CostRevenueCat, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Title)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Title = s }).Select(s => new Func<CostRevenueCat, bool>(p =>true));

            Initialize(new CostRevenueCatPage(), new CostRevenueCatRepository(), searchModel, dynamicFilter);
        }
    }
}
