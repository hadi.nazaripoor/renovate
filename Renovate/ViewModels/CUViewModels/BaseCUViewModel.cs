﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Repositories;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reactive;
using System.Reactive.Linq;

namespace Renovate.ViewModels.CUViewModels
{
    public class BaseCUViewModel<T> : BaseViewModel<T> where T : BaseModel, new()
    {
        public BaseCUViewModel()
        {
        }

        public void Initialize(BaseRepository<T> repository, long modelKeyId = 0, bool containsDeleted = false)
        {
            var modelAttributes = typeof(T).GetAttribute<ModelAttribute>();
            IsNew = modelKeyId == 0;
            ViewTitle = $"تعریف {modelAttributes.SingleName}";
            ViewTitle = IsNew ? $"تعریف {modelAttributes.SingleName} جدید" : $"ویرایش {modelAttributes.SingleName}";

            if (IsNew)
                StayOpenTitle = $"صفحه برای وارد کردن {modelAttributes.SingleName} جدید باز بماند ؟";

            Repository = repository;

            ReturnCommand = ReactiveCommand.Create<Unit, List<T>>(_ =>
            {
                return ChangeSet;
            });

            IDisposable createDisposable = null;
            this.WhenAnyValue(vm => vm.PageModel).Where(pm => pm != null).Subscribe(pm =>
            {
                CreateCommand = ReactiveCommand.Create<Unit>(createdModel =>
                {
                    if (Repository.Insert(PageModel))
                        ChangeSet.Add(PageModel);
                }, PageModel.ValidationContext.WhenAnyValue(context => context.IsValid));

                createDisposable?.Dispose();
                createDisposable = CreateCommand.Subscribe(_ =>
                {
                    if (StayOpen)
                        PageModel = new T();
                    else
                        ReturnCommand.Execute().Subscribe();
                });
            });

            PrepareCommand = ReactiveCommand.Create<Unit>(_ =>
            {
                ChangeSet = new List<T>();
                PageModel = new T();
            });
        }

        private ReactiveCommand<Unit, Unit> createCommand;
        public ReactiveCommand<Unit, Unit> CreateCommand
        {
            get { return createCommand; }
            set { this.RaiseAndSetIfChanged(ref createCommand, value); }
        }

        public ReactiveCommand<T, T> UpdateCommand { get; set; }
        public ReactiveCommand<Unit, List<T>> ReturnCommand { get; set; }
        public ReactiveCommand<Unit, Unit> PrepareCommand { get; set; }

        public List<T> ChangeSet { get; set; }

        public bool IsNew { get; set; }
        public bool StayOpen { get; set; }
        public string StayOpenTitle { get; set; }

        private T pageModel;
        public T PageModel
        {
            get { return pageModel; }
            set { this.RaiseAndSetIfChanged(ref pageModel, value); }
        }

        public BaseRepository<T> Repository { get; set; }
    }
}
