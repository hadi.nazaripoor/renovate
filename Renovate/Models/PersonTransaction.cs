﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("PersonTransactions")]
    [Model(SingleName = "تراکنش شخص", MultipleName = "تراکنش های اشخاص")]
    public class PersonTransaction : BaseModel
    {
        private int personTransactionId;
        private bool isPayment;
        private int personId;
        private Person person;
        private int? transactionId;
        //private Transaction transaction;
        //private long transactedValue;
        private DateTime transactionDateTime = DateTime.Today;
        //private DateTime nextTransactionDate = DateTime.Today;
        private string comment = "";

        [DataMember]
        [Key]
        public int PersonTransactionId
        {
            get { return personTransactionId; }
            set { this.RaiseAndSetIfChanged(ref personTransactionId, value); }
        }

        [DataMember]
        [Required]
        public bool IsPayment
        {
            get { return isPayment; }
            set { this.RaiseAndSetIfChanged(ref isPayment, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "شخص مرتبط با این تراکنش را انتخاب کنید")]
        [Range(1, int.MaxValue, ErrorMessage = "یکی از اشخاص را انتخاب کنید")]
        public int PersonId
        {
            get { return personId; }
            set { this.RaiseAndSetIfChanged(ref personId, value); }
        }

        //[NotMapped]
        //public string BeforeBalanceValueAsString
        //{
        //    get
        //    {
        //        var tempPerson = PersonRepository.GetSpecial(PersonId);
        //        if (tempPerson != null)
        //        {
        //            var Balance = tempPerson.Balance;
        //            var result = Math.Abs(Balance).ToString("n0");
        //            if (Balance != 0) result += (Balance < 0) ? " بستانکار" : " بدهکار";

        //            return result;
        //        }

        //        return "";
        //    }
        //}

        [ForeignKey(nameof(PersonId))]
        public Person Person
        {
            get { return person; }
            set { this.RaiseAndSetIfChanged(ref person, value); }
        }

        [DataMember]
        public int? TransactionId
        {
            get { return transactionId; }
            set { this.RaiseAndSetIfChanged(ref transactionId, value); }
        }

        //[ForeignKey(nameof(TransactionId))]
        //public Transaction Transaction
        //{
        //    get { return transaction; }
        //    //set { transaction = value; if (value != null) TransactedValue = value.TotalTransacted; NotifyPropertyChanged(nameof(Transaction)); }
        //    set { this.RaiseAndSetIfChanged(ref secondUnitTitle, value); }
        //}

        //[NotMapped]
        //public long TransactedValue
        //{
        //    get { return transactedValue; }
        //    set { this.RaiseAndSetIfChanged(ref transactedValue, value); }
        //}

        [DataMember]
        [Required(ErrorMessage = "تاریخ ایجاد تراکنش را مشخص کنید")]
        public DateTime TransactionDateTime
        {
            get { return transactionDateTime; }
            set { this.RaiseAndSetIfChanged(ref transactionDateTime, value); }
        }

        //[NotMapped]
        //public DateTime NextTransactionDate
        //{
        //    get { return nextTransactionDate; }
        //    set { this.RaiseAndSetIfChanged(ref nextTransactionDate, value); }
        //}

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        public PersonTransaction()
        {
            this.ValidationRule(vm => vm.PersonId, personId => personId > 0, "شخص مورد نظر را انتخاب کنید");
            //this.ValidationRule(vm => vm.TransactedValue, transactedValue => transactedValue > 0, "مبلغ تراکنش را مشخص کنید");
        }

        public override string ToString()
        {
            return Person?.FullName;
        }
    }
}
