﻿using ReactiveUI;
using Renovate.Assets.Values.Enums;

namespace Renovate.Assets.DTOs
{
    public class DateTimeDto : BaseDto
    {
        private int day;
        public int Day
        {
            get { return day; }
            set { this.RaiseAndSetIfChanged(ref day, value); }
        }

        private int month;
        public int Month
        {
            get { return month; }
            set { this.RaiseAndSetIfChanged(ref month, value); }
        }

        private int year;
        public int Year
        {
            get { return year; }
            set { this.RaiseAndSetIfChanged(ref year, value); }
        }
        public bool IsLeapYear
        {
            get { return Year % 4 == 3; }
        }

        private short dayOfWeek;
        public short DayOfWeek
        {
            get { return dayOfWeek; }
            set { this.RaiseAndSetIfChanged(ref dayOfWeek, value); }
        }

        public bool ShowInShortFormat { get; set; } = true;

        public override string ToString()
        {
            if (ShowInShortFormat)
                return $"{year:0000}/{month:00}/{day:00}";
            return $"{(PersianDaysOfWeek)DayOfWeek}، {day} {(PersianMonths)month} {year}";
        }
    }
}
