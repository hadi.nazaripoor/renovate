﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace Renovate.Assets.UI.UserControls
{
    /// <summary>
    /// Interaction logic for TitleBarUserControl.xaml
    /// </summary>
    public partial class TitleBarUserControl : UserControl
    {
        public TitleBarUserControl()
        {
            InitializeComponent();

            menuButton.Click += (s, e) => { MenuClicked?.Invoke(this, null); };
            menuButton.PreviewMouseLeftButtonDown += (s, e) => { e.Handled = e.ClickCount == 2; };
            minimizeButton.Click += (s, e) => { Minimized?.Invoke(this, null); };
            closeButton.Click += (s, e) => { Closed?.Invoke(this, null); };
        }

        public event EventHandler<EventArgs> MenuClicked;
        public event EventHandler<EventArgs> Minimized;
        public event EventHandler<EventArgs> Closed;

        public bool IsDialog
        {
            get { return (bool)base.GetValue(IsDialogProperty); }
            set { base.SetValue(IsDialogProperty, value); }
        }
        public static readonly DependencyProperty IsDialogProperty = DependencyProperty.Register("IsDialog", typeof(bool), typeof(TitleBarUserControl), new FrameworkPropertyMetadata(false));
    }
}
