﻿using ReactiveUI;
using System.Windows.Input;

namespace Renovate.Assets.DTOs
{
    public class ShortcutDto : BaseDto
    {
        private ModifierKeys modifierKeys;
        private Key key;
        private string shortcutOutput;
        private bool isValidShortcut;

        public ModifierKeys ModifierKeys
        {
            get { return modifierKeys; }
            set { this.RaiseAndSetIfChanged(ref modifierKeys, value); }
        }

        public Key Key
        {
            get { return key; }
            set { this.RaiseAndSetIfChanged(ref key, value); RefreshResult(); }
        }

        public string ShortcutOutput
        {
            get { return shortcutOutput; }
            set { this.RaiseAndSetIfChanged(ref shortcutOutput, value); }
        }

        public bool IsValidShortcut
        {
            get { return isValidShortcut; }
            set { this.RaiseAndSetIfChanged(ref isValidShortcut, value); }
        }

        private void RefreshResult()
        {
            var keyAsString = "";
            IsValidShortcut = true;

            if (Key >= Key.F1 && Key <= Key.F19)
                keyAsString = Key.ToString();

            else if (Key >= Key.D0 && Key <= Key.D9)
                keyAsString = ((char)(48 + Key - Key.D0)).ToString();

            else if (Key >= Key.NumPad0 && Key <= Key.NumPad9)
                keyAsString = ((char)(48 + Key - Key.NumPad0)).ToString();

            else if (Key >= Key.A && Key <= Key.Z)
                keyAsString = ((char)(65 + Key - Key.A)).ToString();

            else if (Key == Key.Decimal || Key == Key.OemPeriod)
                keyAsString = ".";

            else if (Key == Key.Space)
                keyAsString = Key.ToString();

            ModifierKeys &= (ModifierKeys.Alt | ModifierKeys.Control);

            if (keyAsString == "" || (ModifierKeys == 0 && (Key <= Key.F1 || Key >= Key.F19)))
            {
                ShortcutOutput = "";
                IsValidShortcut = false;
            }
            else
            {
                ShortcutOutput = ModifierKeys.ToString().Replace(", ", " + ");
                if (ShortcutOutput == "None") ShortcutOutput = "";
                ShortcutOutput += ShortcutOutput == "" ? keyAsString : " + " + keyAsString;
            }
        }
    }
}
