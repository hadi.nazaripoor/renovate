﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;
using Renovate.Assets.Values.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Windows.Input;

namespace Renovate.Models
{
    [DataContract]
    [Table("Shortcuts")]
    [Model(SingleName = "کلید میانبر", MultipleName = "کلیدهای میانبر")]
    public class Shortcut : BaseModel
    {
        private PagesOrOperations pageOrOperation;
        private ModifierKeys modifierKeys;
        private short keyCode;
        private bool isInQuickAccess;

        [DataMember]
        [Key]
        public PagesOrOperations PagesOrOperations
        {
            get { return pageOrOperation; }
            set { this.RaiseAndSetIfChanged(ref pageOrOperation, value); }
        }

        [DataMember]
        [Required]
        public ModifierKeys ModifierKeys
        {
            get { return modifierKeys; }
            set { this.RaiseAndSetIfChanged(ref modifierKeys, value); }
        }

        [DataMember]
        [Required]
        public short KeyCode
        {
            get { return keyCode; }
            set { this.RaiseAndSetIfChanged(ref keyCode, value); }
        }

        [DataMember]
        [Required]
        public bool IsInQuickAccess
        {
            get { return isInQuickAccess; }
            set { this.RaiseAndSetIfChanged(ref isInQuickAccess, value); }
        }

        [NotMapped]
        public string ShortcutString
        {
            get { return new ShortcutDto { ModifierKeys = (ModifierKeys)this.ModifierKeys, Key = (Key)this.KeyCode }.ShortcutOutput; }
        }

        public Shortcut()
        {
        }
    }
}
