﻿using Renovate.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Renovate.Assets.Extensions
{
    public static class IEnumerableExtension
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> list)
        {
            return list == null || list.Count() == 0;
        }

        public static ObservableCollection<BaseModel> ToBaseModelObservableCollection<T>(this IEnumerable<T> list) where T : BaseModel
        {
            if (list.IsNullOrEmpty())
                return null;

            return new ObservableCollection<BaseModel>(list.Select(item => (BaseModel)item));
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> list)
        {
            if (list.IsNullOrEmpty())
                return null;

            return new ObservableCollection<T>(list);
        }
    }
}
