﻿using System.ComponentModel.DataAnnotations;

namespace Renovate.Assets.Values.Enums
{
    public enum PersianDaysOfWeek
    {
        [Display(Name = "شنبه")]
        Saturday = 1,
        [Display(Name = "یکشنبه")]
        Sunday = 2,
        [Display(Name = "دوشنبه")]
        Monday = 3,
        [Display(Name = "سه_شنبه")]
        Tuesday = 4,
        [Display(Name = "چهارشنبه")]
        Wednesday = 5,
        [Display(Name = "پنجشنبه")]
        Thursday = 6,
        [Display(Name = "جمعه")]
        Friday = 7
    }
}
