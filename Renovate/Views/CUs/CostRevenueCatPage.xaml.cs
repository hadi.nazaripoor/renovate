﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Repositories;
using System.Reactive.Linq;
using System;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for CostRevenueCatPage.xaml
    /// </summary>
    public partial class CostRevenueCatPage : BaseCuPage<CostRevenueCat>
    {
        public CostRevenueCatPage()
        {
            InitializeComponent();
            Initialize(new CostRevenueCatRepository());

            ViewModel.WhenAnyValue(vm => vm.PageModel).Where(pm => pm != null).Subscribe(_ =>
            {
                FirstEntryControl.ItemsSource = new CostRevenueCatRepository().Get().GetHierarchyObservableCollection().ToBaseModelObservableCollection();
            });
        }
    }
}
