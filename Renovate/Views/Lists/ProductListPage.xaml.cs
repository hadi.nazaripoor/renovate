﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for ProductListPage.xaml
    /// </summary>
    public partial class ProductListPage : BaseListPage<Product>
    {
        public ProductListPage()
        {
            InitializeComponent();
            var searchModel = new ProductSearchModel();

            IObservable<Func<Product, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Name, s => s.ProductCatId)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Name = s.Item1, ProductCatId = s.Item2 })
                .Select(s => new Func<Product, bool>(p =>
                    (s.Name.IsNullOrWhiteSpace() || p.Name.Contains(s.Name)) &&
                    (s.ProductCatId == 0 || p.ProductCatId == s.ProductCatId)));

            Initialize(new ProductPage(), new ProductRepository(), searchModel, dynamicFilter);
        }
    }
}
