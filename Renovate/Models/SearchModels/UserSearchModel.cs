﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
    public class UserSearchModel : SearchBaseDto
    {
		private string username;

		[Search(Title = "نام کاربری")]
		public string Username
		{
			get { return username; }
			set { this.RaiseAndSetIfChanged(ref username, value); }
		}
	}
}
