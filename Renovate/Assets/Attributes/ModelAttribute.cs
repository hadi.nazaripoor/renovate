﻿using System;

namespace Renovate.Assets.Attributes
{
    public class ModelAttribute : Attribute
    {
        public string SingleName { get; set; }
        public string MultipleName { get; set; }
    }
}
