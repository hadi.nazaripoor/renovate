﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("AccountTransitions")]
    [Model(SingleName = "انتقال وجه بین حساب ها", MultipleName = "انتقال های وجوه بین حساب ها")]
    public class AccountTransition : BaseModel
    {
        private int accountTransitionId;

        private short sourceAccountId;
        private Account sourceAccount;

        private long transmittedvalue;

        private short destinationAcconutId;
        private Account destinationAccount;

        private int? transactionId;
        //private Transaction transaction;

        private DateTime transitionDateTime = DateTime.Today;
        private string comment = "";

        [DataMember]
        [Key]
        public int AccountTransitionId
        {
            get { return accountTransitionId; }
            set { this.RaiseAndSetIfChanged(ref accountTransitionId, value); }
        }

        [DataMember]
        [Required]
        [Range(1, short.MaxValue)]
        public short SourceAccountId
        {
            get { return sourceAccountId; }
            set { this.RaiseAndSetIfChanged(ref sourceAccountId, value); }
        }

        //private void ValidateSourceAndDestinationAccount()
        //{
        //    var validationMessage = "هر دو حساب یکی هستند";

        //    if (SourceAccountId == DestinationAccountId)
        //        AddError(nameof(DestinationAccountId), validationMessage);
        //    else
        //        RemoveError(nameof(DestinationAccountId), validationMessage);

        //    //if (SourceAccountId == DestinationAccountId)
        //    //{
        //    //    if (errors.ContainsKey(nameof(DestinationAccountId)))
        //    //        errors[nameof(DestinationAccountId)].Add(validationMessage);
        //    //    else
        //    //        errors[nameof(DestinationAccountId)] = new List<string> { validationMessage };
        //    //    OnErrorsChanged(nameof(DestinationAccountId));
        //    //}
        //    //else if (errors.ContainsKey(nameof(DestinationAccountId)))
        //    //{
        //    //    errors[nameof(DestinationAccountId)].Remove(validationMessage);

        //    //    if (errors[nameof(DestinationAccountId)].Count == 0)
        //    //    {
        //    //        List<string> outLi;
        //    //        errors.TryRemove(nameof(DestinationAccountId), out outLi);
        //    //        OnErrorsChanged(nameof(DestinationAccountId));
        //    //    }
        //    //}
        //}

        [ForeignKey(nameof(SourceAccountId))]
        public Account SourceAccount
        {
            get { return sourceAccount; }
            set { this.RaiseAndSetIfChanged(ref sourceAccount, value); }
        }

        [DataMember]
        [Required]
        [Range(1, long.MaxValue, ErrorMessage = "حداقل مقدار جابجایی وجه بین حساب ها باید بیشتر از صفر باشد")]
        public long TransmittedValue
        {
            get { return transmittedvalue; }
            set { this.RaiseAndSetIfChanged(ref transmittedvalue, value); }
        }

        [DataMember]
        [Required]
        [Range(1, short.MaxValue)]
        public short DestinationAccountId
        {
            get { return destinationAcconutId; }
            set { this.RaiseAndSetIfChanged(ref destinationAcconutId, value); }
        }

        [ForeignKey(nameof(DestinationAccountId))]
        public Account DestinationAccount
        {
            get { return destinationAccount; }
            set { this.RaiseAndSetIfChanged(ref destinationAccount, value); }
        }

        [NotMapped]
        public string AccountTransitionTitle { get { return $"انتقال وجه از {SourceAccount.Title} به {DestinationAccount.Title}"; } }

        [DataMember]
        public int? TransactionId
        {
            get { return transactionId; }
            set { this.RaiseAndSetIfChanged(ref transactionId, value); }
        }

        //[ForeignKey(nameof(TransactionId))]
        //public Transaction Transaction
        //{
        //    get { return transaction; }
        //    set { transaction = value; NotifyPropertyChanged(nameof(Transaction)); }
        //    set { this.RaiseAndSetIfChanged(ref countUnitId, value); }
        //}

        [DataMember]
        [Required]
        public DateTime TransactionDateTime
        {
            get { return transitionDateTime; }
            set { this.RaiseAndSetIfChanged(ref transitionDateTime, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        public AccountTransition()
        {
            this.ValidationRule(vm => vm.SourceAccountId, sourceAccountId => sourceAccountId > 0, "حساب مبدا را مشخص کنید");
            this.ValidationRule(vm => vm.DestinationAccountId, destinationAccountId => destinationAccountId > 0, "حساب مقصد را مشخص کنید");
            this.ValidationRule(vm => vm.TransmittedValue, transmitedValue => transmitedValue > 0, "مبلغ انتقال را مشخص کنید");
        }
    }
}
