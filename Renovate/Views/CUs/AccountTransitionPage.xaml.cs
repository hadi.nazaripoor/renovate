﻿using Renovate.Models;
using Renovate.Repositories;
using System.Reactive.Linq;
using System;
using Renovate.Assets.Extensions;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for AccountTransitionPage.xaml
    /// </summary>
    public partial class AccountTransitionPage : BaseCuPage<AccountTransition>
    {
        public AccountTransitionPage()
        {
            InitializeComponent();
            Initialize(new AccountTransitionRepository());

            Observable.FromEventPattern(this, nameof(this.Loaded)).Subscribe(_ =>
            {
                FirstEntryControl.ItemsSource = new AccountRepository().Get().ToObservableCollection();
                DestinationAccountComboBox.ItemsSource = new AccountRepository().Get().ToObservableCollection();
            });
        }
    }
}
