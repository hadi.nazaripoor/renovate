﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
    public class CountUnitSearchModel : SearchBaseDto
    {
		private string countUnitTitle;

		[Search(Title = "عنوان واحد شمارش")]
		public string CountUnitTitle
		{
			get { return countUnitTitle; }
			set { this.RaiseAndSetIfChanged(ref countUnitTitle, value); }
		}
	}
}
