﻿using Microsoft.Win32;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using Renovate.Models;
using Renovate.Repositories;
using System;
using System.IO;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for UserPage.xaml
    /// </summary>
    public partial class UserPage : BaseCuPage<User>
    {
        public UserPage()
        {
            InitializeComponent();

            FirstEntryControl.ItemsSource = new PersonRepository().Get().ToObservableCollection();
            ConfirmButton.Click += (s, e) =>
            {
                ViewModel.PageModel.Password = PasswordPasswordBox.Password;
                ViewModel.PageModel.CreateDateTime = DateTime.Now;

                if (!ViewModel.PageModel.HasErrors)
                    ViewModel.CreateCommand.Execute().Subscribe();
            };
            AddImageButton.Click += (s, e) =>
            {
                var openFileDialog = new OpenFileDialog()
                {
                    Filter = "Image Files(*.jpg, *.jpeg, *.bmp, *.png) | *.jpg; *.jpeg; *.bmp; *.png"
                };

                var result = openFileDialog.ShowDialog();
                if (result.HasValue && result.Value)
                {
                    ViewModel.PageModel.AvatarImageBytes = File.ReadAllBytes(openFileDialog.FileName);
                }
            };
            RemoveImageButton.Click += (s, e) => { ViewModel.PageModel.AvatarImageBytes = null; };

            Initialize(new UserRepository());
        }
    }
}
