﻿using Renovate.Assets.DTOs;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Renovate.Assets.Extensions
{
    public static class EnumExtensions
    {
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue) where TAttribute : Attribute
        {
            return enumValue.GetType().GetMember(enumValue.ToString())
                            .First().GetCustomAttribute<TAttribute>();
        }

        public static ObservableCollection<EnumDto> ToCollection(this Type enumType, bool hasDontCareItem = false)
        {
            var enumValues = Enum.GetValues(enumType);

            return (from object enumValue in enumValues select new EnumDto(enumValue, GetDescription((Enum)enumValue))).ToObservableCollection();
        }

        private static string GetDescription(Enum enumValue)
        {
            return enumValue.GetType().GetMember(enumValue.ToString()).First().GetCustomAttribute<DisplayAttribute>().GetName();
        }
    }
}
