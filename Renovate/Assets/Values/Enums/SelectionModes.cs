﻿namespace Renovate.Assets.Values.Enums
{
    public enum SelectionModes
    {
        NoItemSelected,
        SomeItemsSelected,
        AllItemsSelected
    }
}
