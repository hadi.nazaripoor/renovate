﻿using System.ComponentModel.DataAnnotations;

namespace Renovate.Assets.Values.Enums
{
    public enum ExitTypes
    {
        [Display(Name = "خروج به دلایل نامعلوم")]
        UnknwonReason = 1,
        [Display(Name = "خروج عادی با پشتیبان گیری")]
        NormalExitWithBackup = 2,
        [Display(Name = "خروج عادی بدون پشتبان گیری")]
        NormalExitNoBackup = 3,
        [Display(Name = "قفل نرم افزار")]
        Lock = 4,
    }
}
