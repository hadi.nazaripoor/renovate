﻿using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renovate.Assets.UI.UserControls
{
    /// <summary>
    /// Interaction logic for TreeViewUserControl.xaml
    /// </summary>
    public partial class TreeViewUserControl : UserControl
    {
        public TreeViewUserControl()
        {
            InitializeComponent();
            this.LostFocus += (s, e) => { TreeViewPopup.StaysOpen = false; };
            this.GotFocus += (s, e) => { TreeViewPopup.StaysOpen = true; };
            ItemsTreeView.SelectedItemChanged += (s, e) => 
            {
                var selectedItem = (BaseModel)ItemsTreeView.SelectedItem;
                SelectedValue = KeyProperty.GetValue(selectedItem);
                SelectionChanged?.Invoke(this, selectedItem);
                TreeViewPopup.IsOpen = false;
            };
            ToggleVisibilityButton.Click += (s, e) => { TreeViewPopup.IsOpen = !TreeViewPopup.IsOpen; };
        }

        public PropertyInfo KeyProperty { get; set; }
        public PropertyInfo ParentProperty { get; set; }
        public event EventHandler<BaseModel> SelectionChanged;

        public object SelectedValue
        {
            get { return (object)GetValue(SelectedValueProperty); }
            set { SetValue(SelectedValueProperty, value); }
        }
        public static readonly DependencyProperty SelectedValueProperty = DependencyProperty.Register("SelectedValue", typeof(object), typeof(TreeViewUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, SelectionChange));

        public string Title
        {
            get { return (string)base.GetValue(TitleProperty); }
            set { base.SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(TreeViewUserControl), new PropertyMetadata(""));
        private static void SelectionChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var treeViewUC = d as TreeViewUserControl;
            var itemsSource = treeViewUC.ItemsSource;

            if (treeViewUC.SelectedValue != null && itemsSource != null && itemsSource.Count > 0)
            {
                var selectedItem = itemsSource.SearchInHierarchicalCollection(treeViewUC.KeyProperty, treeViewUC.SelectedValue);
                if (selectedItem != null)
                    treeViewUC.SelectedItemTitleTextBox.Text = selectedItem.Item1.ToString();
            }
        }

        public ObservableCollection<BaseModel> ItemsSource
        {
            get { return (ObservableCollection<BaseModel>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); SetProperties(); }
        }
        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register("ItemsSource", typeof(ObservableCollection<BaseModel>), typeof(TreeViewUserControl), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        private void SetProperties()
        {
            IsEnabled = ItemsSource != null && ItemsSource.Count > 0;

            if (IsEnabled)
            {
                var itemType = ItemsSource.FirstOrDefault().GetType();

                KeyProperty = itemType.GetKeyProperty();
                ParentProperty = itemType.GetProperties().FirstOrDefault(p => p.PropertyType == itemType);
            }
        }
    }
}
