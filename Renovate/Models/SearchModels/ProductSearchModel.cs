﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
    public class ProductSearchModel : SearchBaseDto
    {
		private string name;
		private short productCatId;

		[Search(Title = "نام")]
		public string Name
		{
			get { return name; }
			set { this.RaiseAndSetIfChanged(ref name, value); }
		}

		[Search(Title = "دسته محصول یا خدمات")]
		public short ProductCatId
		{
			get { return productCatId; }
			set { this.RaiseAndSetIfChanged(ref productCatId, value); }
		}
	}
}
