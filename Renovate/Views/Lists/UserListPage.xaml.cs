﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for UserListPage.xaml
    /// </summary>
    public partial class UserListPage : BaseListPage<User>
    {
        public UserListPage()
        {
            InitializeComponent();
            var searchModel = new UserSearchModel();

            IObservable<Func<User, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Username)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(username => new Func<User, bool>(u => (username.IsNullOrWhiteSpace() || u.Username.Contains(username))));

            Initialize(new UserPage(), new UserRepository(), searchModel, dynamicFilter);
        }
    }
}
