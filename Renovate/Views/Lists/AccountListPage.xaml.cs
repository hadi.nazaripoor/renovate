﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for AccountListPage.xaml
    /// </summary>
    public partial class AccountListPage : BaseListPage<Account>
    {
        public AccountListPage()
        {
            InitializeComponent();
            var searchModel = new AccountSearchModel();

            IObservable<Func<Account, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.AccountTitle)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(title => new Func<Account, bool>(p =>title.IsNullOrWhiteSpace() || p.Title.Contains(title)));

            Initialize(new AccountPage(), new AccountRepository(), searchModel, dynamicFilter);
        }
    }
}
