﻿using Renovate.Assets.DTOs;
using Renovate.Assets.Extensions;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Renovate.Assets.UI.Converters
{
    class DateConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((DateTime)value).ToPersian<DateTimeDto>();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
