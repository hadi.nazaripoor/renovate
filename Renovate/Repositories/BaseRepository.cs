﻿using Renovate.Assets.Extensions;
using Renovate.Assets.Utilities;
using Renovate.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;

namespace Renovate.Repositories
{
    //public interface IBaseRepository<T> where T : BaseModel
    //{
    //    IEnumerable<T> Get(T selector = null);
    //    T GetSingle(dynamic key, T selector = null);
    //    IEnumerable<T> Find(string searchCriteria, T selector = null);
    //    bool Insert(T modelToInsert);
    //    int Delete(IEnumerable<T> itemsToDelete);
    //}

    public class BaseRepository<T> where T : BaseModel
    {
        public IEnumerable<T> Get(T selector = null)
        {
            var columnsString = selector?.GetSelector();
            return SqlUtil.ExecuteReader<T>($"Select {columnsString ?? "*"} From {typeof(T).GetAttribute<TableAttribute>().Name}");
        }

        public T GetSingle(dynamic key, T selector = null)
        {
            var keyPropertyName = typeof(T).GetKeyPropertyName();
            var columnsString = selector?.GetSelector();

            return SqlUtil.ExecuteReader<T>($"Select Top(1) {columnsString ?? "*"} From {typeof(T).GetAttribute<TableAttribute>().Name} Where {keyPropertyName} = '{key}'").FirstOrDefault();
        }

        public IEnumerable<T> Find(string searchCriteria, T selector = null)
        {
            var columnsString = selector?.GetSelector();

            return SqlUtil.ExecuteReader<T>($"Select Top(1) {columnsString ?? "*"} From {typeof(T).GetAttribute<TableAttribute>().Name} Where {searchCriteria}'");
        }

        public virtual bool Insert(T modelToInsert)
        {
            var modelKeyValues = modelToInsert?.GetKeyValuePairs();
            var keyPropertyName = typeof(T).GetKeyPropertyName();
            modelKeyValues.RemoveAll(kvp => kvp.Key == keyPropertyName);
            var parameters = modelKeyValues.Select(keyValue => new SqlParameter(keyValue.Key, keyValue.Value)).ToList();

            return SqlUtil.ExecuteNonQuery($"Insert Into {typeof(T).GetAttribute<TableAttribute>().Name} ({modelKeyValues.Select(m => m.Key).Join(", ")}) Values ({modelKeyValues.Select(m => $"@{m.Key}").Join(", ")})", commandParameters: parameters) > 0;
        }

        public int Delete(IEnumerable<T> itemsToDelete)
        {
            var keyPropertyName = typeof(T).GetKeyPropertyName();
            var keyProperty = typeof(T).GetProperty(keyPropertyName);
            var keys = itemsToDelete.Select(item => $"'{keyProperty.GetValue(item)}'");

            return SqlUtil.ExecuteNonQuery($"Delete From {typeof(T).GetAttribute<TableAttribute>().Name} Where {keyPropertyName} In ({keys.Join(", ")})");
        }

        public BaseRepository()
        {
        }
    }
}
