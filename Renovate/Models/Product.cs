﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("Products")]
    [Model(SingleName = "کالا", MultipleName = "کالا ها")]
    public class Product : BaseModel
    {
        private int productId;
        private string name;
        private short productCatId;

        private double orderPoint;
        private double primaryCount;
        private double currentCount;
        private short countUnitId;

        private ProductTradeTypes tradeType;

        private long lastBuyPrice;
        private long salePrice;

        private string comment = "";

        private bool isDeleted;

        [DataMember]
        [Key]
        public int ProductId
        {
            get { return productId; }
            set { this.RaiseAndSetIfChanged(ref productId, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "نام محصول نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Name
        {
            get { return name; }
            set { this.RaiseAndSetIfChanged(ref name, value); }
        }

        [DataMember]
        [Required]
        [Range(1, short.MaxValue, ErrorMessage = "یکی از دسته های محصول را انتخاب کنید")]
        public short ProductCatId
        {
            get { return productCatId; }
            set { this.RaiseAndSetIfChanged(ref productCatId, value); }
        }

        [ForeignKey(nameof(ProductCatId))]
        public ProductCat ProductCat { get; set; }

        [DataMember]
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "نقطه سفارش نمی تواند منفی باشد")]
        public double OrderPoint
        {
            get { return orderPoint; }
            set { this.RaiseAndSetIfChanged(ref orderPoint, value); }
        }

        [DataMember]
        [Required]
        [Range(0, int.MaxValue, ErrorMessage = "موجودی اولیه نمی تواند منفی باشد")]
        public double PrimaryCount
        {
            get { return primaryCount; }
            set { this.RaiseAndSetIfChanged(ref primaryCount, value); }
        }

        [DataMember]
        [Required]
        public double CurrentCount
        {
            get { return currentCount; }
            set { this.RaiseAndSetIfChanged(ref currentCount, value); }
        }

        [DataMember]
        [Required]
        [Range(1, short.MaxValue, ErrorMessage = "یکی از واحدهای شمارش را انتخاب کنید")]
        public short CountUnitId
        {
            get { return countUnitId; }
            set { this.RaiseAndSetIfChanged(ref countUnitId, value); }
        }

        [ForeignKey(nameof(CountUnitId))]
        public CountUnit CountUnit { get; set; }

        [DataMember]
        [Required]
        public ProductTradeTypes TradeType
        {
            get { return tradeType; }
            set { this.RaiseAndSetIfChanged(ref tradeType, value); }
        }

        [DataMember]
        [Required]
        [Range(0, long.MaxValue, ErrorMessage = "آخرین قیمت خرید نمی تواند منفی باشد")]
        public long LastBuyPrice
        {
            get { return lastBuyPrice; }
            set { this.RaiseAndSetIfChanged(ref lastBuyPrice, value); }
        }

        [DataMember]
        [Range(0, long.MaxValue, ErrorMessage = "قیمت فروش نمی تواند منفی باشد")]
        [Required]
        public long SalePrice
        {
            get { return salePrice; }
            set { this.RaiseAndSetIfChanged(ref salePrice, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        [DataMember]
        [Required]
        public bool IsDeleted
        {
            get { return isDeleted; }
            set { this.RaiseAndSetIfChanged(ref isDeleted, value); }
        }

        public Product()
        {
            this.ValidationRule(vm => vm.Name, name => !name.IsNullOrWhiteSpace(), "نام نمی تواند خالی باشد");
            this.ValidationRule(vm => vm.CountUnitId, countUnitId => countUnitId > 0, "یک واحد اندازه گیری مشخص کنید");
            this.ValidationRule(vm => vm.ProductCatId, productCatId => productCatId > 0, "یک دسته کالا یا خدمات را مشخص کنید");
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
