﻿using Renovate.Assets.Values.Variables;
using Renovate.Data;
using Renovate.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Renovate
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MainViewModel MainViewModel = new MainViewModel();
        public App()
        {
            this.Startup += (s, e) =>
            {
                DatabaseVariables.ServerName = ".\\Seventeen";
                DatabaseVariables.DatabaseName = "RenovateV1";
                DatabaseVariables.TrustedConnection = true;
                //var p = new DatabaseContext().People.FirstOrDefault();
            };
        }
    }
}
