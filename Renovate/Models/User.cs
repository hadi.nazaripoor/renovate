﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.Utilities;
using Renovate.Assets.Values.Constants;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("Users")]
    [Model(SingleName = "کاربر", MultipleName = "کاربران")]
    public class User : BaseModel
    {
        private string username;
        private string hashedPassword;
        private DateTime createDateTime;
        private int? personId;
        private byte[] avatarImageBytes;

        private bool isDeleted;

        [DataMember]
        [Key]
        [Required(ErrorMessage = "نام کاربری نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Username
        {
            get { return username; }
            set { this.RaiseAndSetIfChanged(ref username, value); }
        }

        [DataMember]
        public int? PersonId
        {
            get { return personId; }
            set { this.RaiseAndSetIfChanged(ref personId, value); }
        }

        [ForeignKey(nameof(PersonId))]
        public Person Person { get; set; }


        [NotMapped]
        public string Password
        {
            get { return ""; } // Not Important
            set { HashedPassword = CryptoUtil.GenerateSaltedHashBytes(value); }
        }

        [DataMember]
        [Required]
        [MaxLength(LengthConstants.LARGE_STRING)]
        public string HashedPassword
        {
            get { return hashedPassword; }
            set { this.RaiseAndSetIfChanged(ref hashedPassword, value); }
        }

        [DataMember]
        [Required]
        public DateTime CreateDateTime
        {
            get { return createDateTime; }
            set { this.RaiseAndSetIfChanged(ref createDateTime, value); }
        }

        [DataMember]
        [Column(TypeName = "Image")]
        public byte[] AvatarImageBytes
        {
            get { return avatarImageBytes; }
            set { this.RaiseAndSetIfChanged(ref avatarImageBytes, value); }
        }

        [DataMember]
        [Required]
        public bool IsDeleted
        {
            get { return isDeleted; }
            set { this.RaiseAndSetIfChanged(ref isDeleted, value); }
        }

        public User()
        {
        }

        public override string ToString()
        {
            return Person?.FullName ?? Username;
        }
    }
}
