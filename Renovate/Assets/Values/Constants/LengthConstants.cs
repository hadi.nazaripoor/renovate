﻿namespace Renovate.Assets.Values.Constants
{
    public class LengthConstants
    {
        public const int CALL_NUMBERS = 11,
                         SMALL_STRING = 16,
                         MEDIUM_STRING = 64,
                         LARGE_STRING = 256,
                         VERY_LARGE_STRING = 2048;
    }
}
