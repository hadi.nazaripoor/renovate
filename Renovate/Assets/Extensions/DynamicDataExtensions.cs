﻿using DynamicData.Binding;
using System;

namespace Renovate.Assets.Extensions
{
    public static class DynamicDataExtensions
    {
        public static void ForEach<T>(this ObservableCollectionExtended<T> source, Action<T> action)
        {
            for (int i = 0; i < source.Count; i++)
                action(source[i]);
        }
    }
}
