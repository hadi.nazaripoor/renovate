﻿using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using Renovate.Models;
using Renovate.Repositories;
using System.Reactive.Linq;
using System;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for ProductPage.xaml
    /// </summary>
    public partial class ProductPage : BaseCuPage<Product>
    {
        public ProductPage()
        {
            InitializeComponent();
            Initialize(new ProductRepository());

            Observable.FromEventPattern(this, nameof(this.Loaded)).Subscribe(_ =>
            {
                ProductCatComboBox.ItemsSource = new ProductCatRepository().Get().ToObservableCollection();
                CountUnitComboBox.ItemsSource = new CountUnitRepository().Get().ToObservableCollection();
                TradeTypesComboBox.ItemsSource = typeof(ProductTradeTypes).ToCollection();
            });
        }
    }
}
