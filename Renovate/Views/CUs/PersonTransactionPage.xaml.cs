﻿using Renovate.Models;
using Renovate.Repositories;
using System.Reactive.Linq;
using System;
using Renovate.Assets.Extensions;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for PersonTransactionPage.xaml
    /// </summary>
    public partial class PersonTransactionPage : BaseCuPage<PersonTransaction>
    {
        public PersonTransactionPage()
        {
            InitializeComponent();
            Initialize(new PersonTransactionRepository());

            Observable.FromEventPattern(this, nameof(this.Loaded)).Subscribe(_ =>
            {
                FirstEntryControl.ItemsSource = new PersonRepository().Get().ToObservableCollection();
            });
        }
    }
}
