﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
    public class NoteSearchModel : SearchBaseDto
    {
		private string subject;

		[Search(Title = "موضوع یادداشت")]
		public string Subject
		{
			get { return subject; }
			set { this.RaiseAndSetIfChanged(ref subject, value); }
		}
	}
}
