﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for CountUnitPage.xaml
    /// </summary>
    public partial class CountUnitListPage : BaseListPage<CountUnit>
    {
        public CountUnitListPage()
        {
            InitializeComponent();
            var searchModel = new CountUnitSearchModel();

            IObservable<Func<CountUnit, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.CountUnitTitle)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(countUnitTitle => new Func<CountUnit, bool>(cu => countUnitTitle.IsNullOrWhiteSpace() || cu.FirstUnitTitle.Contains(countUnitTitle) || cu.SecondUnitTitle.Contains(countUnitTitle)));

            Initialize(new CountUnitPage(), new CountUnitRepository(), searchModel, dynamicFilter);
        }
    }
}