﻿using Renovate.Assets.Values.Enums;
using System;

namespace Renovate.Assets.UI.CustomEventArgs
{
    public class DialogEventArgs : EventArgs
    {
        public DialogResults DialogResult { get; set; }
    }
}
