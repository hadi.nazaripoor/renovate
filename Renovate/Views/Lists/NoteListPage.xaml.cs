﻿using ReactiveUI;
using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for NoteListPage.xaml
    /// </summary>
    public partial class NoteListPage : BaseListPage<Note>
    {
        public NoteListPage()
        {
            InitializeComponent();
            var searchModel = new NoteSearchModel();

            IObservable<Func<Note, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Subject)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(subject => new Func<Note, bool>(p => subject.IsNullOrWhiteSpace() || p.Subject.Contains(subject)));

            Initialize(new NotePage(), new NoteRepository(), searchModel, dynamicFilter);
        }
    }
}
