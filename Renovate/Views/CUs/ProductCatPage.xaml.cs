﻿using Renovate.Assets.Extensions;
using Renovate.Models;
using Renovate.Repositories;
using System.Reactive.Linq;
using System;
using ReactiveUI;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for ProductCatPage.xaml
    /// </summary>
    public partial class ProductCatPage : BaseCuPage<ProductCat>
    {
        public ProductCatPage()
        {
            InitializeComponent();
            Initialize(new ProductCatRepository());

            ViewModel.WhenAnyValue(vm => vm.PageModel).Where(pm => pm != null).Subscribe(_ =>
            {
                FirstEntryControl.ItemsSource = new ProductCatRepository().Get().GetHierarchyObservableCollection().ToBaseModelObservableCollection();
            });
        }
    }
}
