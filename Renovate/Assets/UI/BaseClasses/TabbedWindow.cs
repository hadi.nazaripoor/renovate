﻿using ReactiveUI;
using Renovate.Assets.UI.UserControls;
using System;
using System.Reactive.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Renovate.Assets.UI.BaseClasses
{
    public class TabbedWindow : Window
    {
        public TitleBarUserControl TitleBar { get; set; }
        public TabbedWindow()
        {
            base.Initialized += (sender, eventArgs) =>
            {
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.FontFamily = new FontFamily("Vazir FD");
                this.FontSize = 14;

                TitleBar = new TitleBarUserControl();
                TitleBar.Closed += (s, e) => { this.WindowStyle = WindowStyle.SingleBorderWindow; this.Close(); };
                TitleBar.Minimized += (s, e) => { this.WindowStyle = WindowStyle.SingleBorderWindow; this.WindowState = WindowState.Minimized; };

                var titleBarPlaceHolder = new ContentControl() { };
                titleBarPlaceHolder.MouseMove += (s, e) => { SendMessage(new WindowInteropHelper(this).Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0); };
                titleBarPlaceHolder.Content = TitleBar;
                titleBarPlaceHolder.MouseDoubleClick += (s, e) => { this.WindowStyle = WindowStyle.SingleBorderWindow; WindowState = WindowState == WindowState.Maximized ? WindowState.Normal : WindowState.Maximized; };

                var newContent = new Grid() { Background = Brushes.Transparent };
                newContent.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(36, GridUnitType.Pixel) });
                newContent.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
                newContent.Children.Add(titleBarPlaceHolder);

                var preContent = this.Content as Grid;
                preContent.SetValue(Grid.RowProperty, 1);
                
                this.Content = null;

                newContent.Children.Add(preContent);

                var borderRectangle = new Rectangle() { Stroke = Brushes.Silver, Visibility = Visibility.Collapsed };
                borderRectangle.SetValue(Grid.RowSpanProperty, 2);
                newContent.Children.Add(borderRectangle);

                this.WhenAnyValue(w => w.WindowState).ObserveOn(SynchronizationContext.Current).Subscribe(ws =>
                {
                    if (ws == WindowState.Maximized)
                    {
                        TitleBar.Margin = new Thickness(8, 8, 8, 0);
                        preContent.Margin = new Thickness(8, 0, 8, 8);
                        borderRectangle.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        TitleBar.Margin = new Thickness(0);
                        preContent.Margin = new Thickness(0);
                        borderRectangle.Visibility = Visibility.Visible;
                    }
                });

                this.Content = newContent;
            };
        }

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        //[DllImportAttribute("user32.dll")]
        //public static extern bool ReleaseCapture();
    }
}
