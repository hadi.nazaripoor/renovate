﻿using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using Renovate.Models;
using Renovate.Repositories;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for CountUnitPage.xaml
    /// </summary>
    public partial class CountUnitPage : BaseCuPage<CountUnit>
    {
        public CountUnitPage()
        {
            InitializeComponent();

            FirstEntryControl.ItemsSource = typeof(CountUnitRelationTypes).ToCollection();

            Initialize(new CountUnitRepository());
        }
    }
}
