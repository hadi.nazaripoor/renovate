﻿using Renovate.Assets.DTOs;
using Renovate.Models;
using Renovate.Repositories;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for ShortcutPage.xaml
    /// </summary>
    public partial class ShortcutPage : BaseCuPage<Shortcut>
    {
        public ShortcutDto ShortcutDto { get; set; } = new ShortcutDto();
        public ShortcutPage()
        {
            InitializeComponent();

            Initialize(new ShortcutRepository());
        }
    }
}
