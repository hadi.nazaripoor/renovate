﻿using ReactiveUI;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for ProductCatListPage.xaml
    /// </summary>
    public partial class ProductCatListPage : BaseListPage<ProductCat>
    {
        public ProductCatListPage()
        {
            InitializeComponent();
            var searchModel = new ProductCatSearchModel();

            var dynamicFilter = searchModel.WhenAnyValue(s => s.Title)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Title = s }).Select(s => new Func<ProductCat, bool>(p => true));

            Initialize(new ProductCatPage(), new ProductCatRepository(), searchModel, dynamicFilter);
        }
    }
}
