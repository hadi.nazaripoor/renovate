﻿using Renovate.Models;
using Renovate.Repositories;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for AccountPage.xaml
    /// </summary>
    public partial class AccountPage : BaseCuPage<Account>
    {
        public AccountPage()
        {
            InitializeComponent();

            Initialize(new AccountRepository());
        }
    }
}
