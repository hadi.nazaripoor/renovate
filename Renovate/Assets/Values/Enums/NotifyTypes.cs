﻿using Renovate.Assets.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Renovate.Assets.Values.Enums
{
    [Flags]
    [EnumAttribute(NonSelectedStringFormat = "هیچ نوع یادآوری انتخاب نشده", SingleSelectedStringFormat = "{0}", MultipleSelectedStringFormat = "{0} نوع یادآوری انتخاب شده")]
    public enum NotifyTypes
    {
        [Display(Name="نیازی به یادآوری نیست")]
        NoNeedToNotify = 1,
        [Display(Name="یادآوری در پایین پنجره")]
        FooterNotify = 2,
        [Display(Name="یادآوری در وسط پنجره")]
        BlockNotify = 4,
        [Display(Name="یادآوری با ارسال پیام")]
        Message = 8
    }
}
