﻿using Renovate.Assets.Values.Enums;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Renovate.Assets.UI.Converters
{
    public class DialogTypesToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            switch ((DialogTypes)value)
            {
                case DialogTypes.Success:
                    return Brushes.Green;
                case DialogTypes.Warning:
                    return Brushes.Orange;
                case DialogTypes.Danger:
                    return Brushes.Red;
                case DialogTypes.Info:
                    return Brushes.Blue;
                default:
                    return Brushes.White;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
