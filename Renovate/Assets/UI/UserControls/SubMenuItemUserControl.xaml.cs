﻿using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using System;
using System.Windows;
using System.Windows.Controls;

namespace Renovate.Assets.UI.UserControls
{
    /// <summary>
    /// Interaction logic for SubMenuUC.xaml
    /// </summary>
    public partial class SubMenuItemUserControl : UserControl
    {
        public PagesOrOperations PageOrOperation
        {
            get { return (PagesOrOperations)base.GetValue(PageOrOperationProperty); }
            set { base.SetValue(PageOrOperationProperty, value); }
        }
        public static readonly DependencyProperty PageOrOperationProperty = DependencyProperty.Register("PageOrOperation", typeof(PagesOrOperations), typeof(SubMenuItemUserControl), new FrameworkPropertyMetadata(PagesOrOperations.None, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, PageOrOperationChanged));

        public NavigationLinkAttribute NavigationLink { get; set; } = new NavigationLinkAttribute();

        public static void PageOrOperationChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var thisInstance = dependencyObject as SubMenuItemUserControl;

            var navigationLink = ((PagesOrOperations)e.NewValue).GetAttribute<NavigationLinkAttribute>();

            if (navigationLink != null)
            {
                thisInstance.NavigationLink.Title = navigationLink.Title;
                thisInstance.NavigationLink.Shortcut = navigationLink.Shortcut;
                thisInstance.NavigationLink.NavigationPageSource = navigationLink.NavigationPageSource;
                thisInstance.NavigationLink.Icon = navigationLink.Icon;
            }
        }


        public SubMenuItemUserControl()
        {
            InitializeComponent();
        }

        public event EventHandler Clicked;

        private void NavigationButton_Click(object sender, RoutedEventArgs e)
        {
            Clicked?.Invoke(this, EventArgs.Empty);
        }
    }
}
