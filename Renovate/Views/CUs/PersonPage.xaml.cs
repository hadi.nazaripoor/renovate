﻿using Renovate.Models;
using Renovate.Repositories;

namespace Renovate.Views.CUs
{
    /// <summary>
    /// Interaction logic for PersonPage.xaml
    /// </summary>
    public partial class PersonPage : BaseCuPage<Person>
    {
        public PersonPage()
        {
            InitializeComponent();
        
            Initialize(new PersonRepository());
        }
    }
}
