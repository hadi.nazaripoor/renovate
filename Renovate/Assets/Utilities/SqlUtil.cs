﻿using Dapper;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Variables;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Renovate.Assets.Utilities
{
    public class SqlUtil
    {
        public static int ExecuteNonQuery(string commandText, string connectionString = "", List<SqlParameter> commandParameters = null)
        {
            var resolvedConnectionString = GetResolvedConnectionString(connectionString);
            var args = new DynamicParameters(new {});
            commandParameters?.ForEach(p => args.Add(p.ParameterName, p.Value));

            using (SqlConnection connection = new SqlConnection(resolvedConnectionString))
            {
                return connection.Execute(commandText, args);
            }
        }

        public static IEnumerable<T> ExecuteReader<T>(string commandText, string connectionString = "", List<SqlParameter> commandParameters = null)
        {
            var resolvedConnectionString = GetResolvedConnectionString(connectionString);
            var args = new DynamicParameters(new { });
            commandParameters?.ForEach(p => args.Add(p.ParameterName, p.Value));

            using (SqlConnection connection = new SqlConnection(resolvedConnectionString))
            {
                return connection.Query<T>(commandText, args);
            }
        }

        private static string GetResolvedConnectionString(string connectionString) => connectionString.IsNullOrWhiteSpace() ? DatabaseVariables.ConnectionString : "" + connectionString;
    }
}
