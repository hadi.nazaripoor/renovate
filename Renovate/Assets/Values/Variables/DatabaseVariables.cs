﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Variables
{
    public class DatabaseVariables
    {
        public static string DatabaseName { get; set; }
        public static string ServerName { get; set; }
        public static bool TrustedConnection { get; set; }
        public static string ConnectionString { get => $"Server={ServerName};Database={DatabaseName};Trusted_Connection={TrustedConnection};"; }

        public DatabaseVariables()
        {
        }
    }
}
