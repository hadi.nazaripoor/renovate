﻿using ReactiveUI;
using Renovate.Models;
using Renovate.Models.SearchModels;
using Renovate.Repositories;
using Renovate.Views.CUs;
using System;
using System.Reactive.Linq;
using System.Threading;

namespace Renovate.Views.Lists
{
    /// <summary>
    /// Interaction logic for ShortcutListPage.xaml
    /// </summary>
    public partial class ShortcutListPage : BaseListPage<Shortcut>
    {
        public ShortcutListPage()
        {
            InitializeComponent();
            var searchModel = new ShortcutSearchModel();

            IObservable<Func<Shortcut, bool>> dynamicFilter = searchModel.WhenAnyValue(s => s.Title)
                .Throttle(TimeSpan.FromMilliseconds(250)).ObserveOn(SynchronizationContext.Current)
                .Select(s => new { Title = s }).Select(s => new Func<Shortcut, bool>(p => true));

            Initialize(new ShortcutPage(), new ShortcutRepository(), searchModel, dynamicFilter);
        }
    }
}
