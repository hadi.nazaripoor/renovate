﻿using System.ComponentModel.DataAnnotations;

namespace Renovate.Assets.Values.Enums
{
    public enum ProductTradeTypes
    {
        [Display(Name = "خرید عمده، فروش عمده")]
        MajorBuy_MajorSale,
        [Display(Name = "خرید عمده، فروش خرده")]
        MajorBuy_MinorSale,
        [Display(Name = "خرید خرده، فروش عمده")]
        MinorBuy_MajorSale,
        [Display(Name = "خرید خرده، فروش خرده")]
        MinorBuy_MinorSale
   }
}
