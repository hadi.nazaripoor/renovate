﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.Values.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("UserPermissions")]
    [Model(SingleName = "سطح دسترسی کاربر", MultipleName = "سطوح دسترسی کاربر")]
    public class UserPermission : BaseModel
    {
        private int userPermissionId;
        private string username;
        private UserPermissionTypes userPermissionType;
        private UserPermissonAccessLevels userPermissionAccessLevels;

        [DataMember]
        [Key]
        public int UserPermissionId
        {
            get { return userPermissionId; }
            set { this.RaiseAndSetIfChanged(ref userPermissionId, value); }
        }

        [DataMember]
        [Required]
        public string Username
        {
            get { return username; }
            set { this.RaiseAndSetIfChanged(ref username, value); }
        }

        [DataMember]
        [Required]
        public UserPermissionTypes UserPermissionType
        {
            get { return userPermissionType; }
            set { this.RaiseAndSetIfChanged(ref userPermissionType, value); }
        }

        [DataMember]
        [Required]
        public UserPermissonAccessLevels UserPermissionAccessLevels
        {
            get { return userPermissionAccessLevels; }
            set { this.RaiseAndSetIfChanged(ref userPermissionAccessLevels, value); }
        }

        public UserPermission()
        {
        }
    }
}
