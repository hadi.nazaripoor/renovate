﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("UserAccessLogs")]
    [Model(SingleName = "سابقه دسترسی", MultipleName = "سوابق دسترسی")]
    public class UserAccessLog : BaseModel
    {
        private long userAccessLogId;
        private string username;
        private DateTime loginDateTime;
        private DateTime logoutDateTime;
        private ExitTypes exitType;

        [DataMember]
        [Key]
        public long UserAccessLogId
        {
            get { return userAccessLogId; }
            set { this.RaiseAndSetIfChanged(ref userAccessLogId, value); }
        }

        [DataMember]
        [Required]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Username
        {
            get { return username; }
            set { this.RaiseAndSetIfChanged(ref username, value); }
        }

        [ForeignKey(nameof(username))]
        public User User { get; set; }

        [DataMember]
        [Required]
        public DateTime LoginDateTime
        {
            get { return loginDateTime; }
            set { this.RaiseAndSetIfChanged(ref loginDateTime, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        public DateTime LogoutDateTime
        {
            get { return logoutDateTime; }
            set { this.RaiseAndSetIfChanged(ref logoutDateTime, value); }
        }

        [DataMember]
        [Required]
        public ExitTypes ExitType
        {
            get { return exitType; }
            set { this.RaiseAndSetIfChanged(ref exitType, value); }
        }

        public UserAccessLog()
        {
        }
    }
}
