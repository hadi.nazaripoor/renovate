﻿using ReactiveUI;
using ReactiveUI.Validation.Helpers;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Renovate.Assets.DTOs
{
    public class BaseDto : ReactiveValidationObject, IDisposable
    {
        #region Dispose
        public void Dispose() { }
        #endregion

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set { this.RaiseAndSetIfChanged(ref isSelected, value); }
        }
    }
}
