﻿using Renovate.Assets.Values.Enums;

namespace Renovate.Assets.DTOs
{
    public class DialogChoiceDto
    {
        public DialogResults DialogResult { get; set; }
        public string Text { get; set; }
    }
}
