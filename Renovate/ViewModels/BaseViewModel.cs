﻿using ReactiveUI;
using Renovate.Models;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Renovate.ViewModels
{
    public class BaseViewModel<T> : ReactiveObject where T : BaseModel, new()
    {
        public BaseViewModel()
        {
        }

        private string viewTitle;
        public string ViewTitle
        {
            get { return viewTitle; }
            set { this.RaiseAndSetIfChanged(ref viewTitle, value); }
        }

        private string itemStatistics;
        public string ItemsStatistics
        {
            get { return itemStatistics; }
            set { this.RaiseAndSetIfChanged(ref itemStatistics, value); }
        }
    }
}
