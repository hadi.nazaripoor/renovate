﻿using Renovate.Assets.DTOs;
using Renovate.Assets.Values.Enums;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Extensions
{
    public static class DateTimeExtensions
    {
        public static short PersianDayOfWeek(DayOfWeek georgianDayOfWeek) => (short)((short)(georgianDayOfWeek + 1) % 7 + 1);
        public static T ToPersian<T>(this DateTime dateTimeToConvert, bool showInShortFormat = true) where T : DateTimeDto, new()
        {
            var persianCalendar = new PersianCalendar();

            var persianDateTime = new T
            {
                Day = persianCalendar.GetDayOfMonth(dateTimeToConvert),
                Month = persianCalendar.GetMonth(dateTimeToConvert),
                Year = persianCalendar.GetYear(dateTimeToConvert),
                DayOfWeek = PersianDayOfWeek(dateTimeToConvert.DayOfWeek),
                ShowInShortFormat = showInShortFormat
            };

            return persianDateTime;
        }

        public static bool TryConvertToDate(this string stringDateToConvert, out DateTime date)
        {
            if (!stringDateToConvert.IsNullOrWhiteSpace())
            {
                string[] dateParts = stringDateToConvert.Split('/');
                if (dateParts.Count() == 3)
                {
                    if (int.TryParse(dateParts[0], out int year) && int.TryParse(dateParts[1], out int month) && int.TryParse(dateParts[2], out int day))
                    {
                        if (year > 0 && month > 0 && day > 0)
                        {
                            try
                            {
                                var persianCalendar = new PersianCalendar();
                                date = persianCalendar.ToDateTime(year, month, day, 0, 0, 0, 0);

                                return true;
                            }
                            catch { }
                        }
                    }
                }
            }

            date = DateTime.Today;
            return false;
        }

        public static void FillMonthInfo(this DateTimeDto dateTimeDto, out int lastDayOfPreMonth, out short firstDayOfWeekForThisMonth, out int thisMonthDaysCount)
        {
            var dateTimeClone = new DateTimeDto { Year = dateTimeDto.Year, Month = dateTimeDto.Month, Day = 1 };

            //
            TryConvertToDate(dateTimeClone.ToString(), out DateTime dateTime);
            dateTimeClone.DayOfWeek = PersianDayOfWeek(dateTime.DayOfWeek);
            //
            firstDayOfWeekForThisMonth = dateTimeClone.DayOfWeek;
            thisMonthDaysCount = (dateTimeClone.Month < 7) ? 31 : 30;
            thisMonthDaysCount = !dateTimeClone.IsLeapYear && (dateTimeClone.Month == 12) ? 29 : thisMonthDaysCount;

            var preMonth = dateTimeClone.Month - 1;
            if (preMonth == 0)
            {
                preMonth += 12;
                dateTimeClone.Year--;
            }

            lastDayOfPreMonth = (preMonth < 7) ? 31 : 30;
            lastDayOfPreMonth = !dateTimeClone.IsLeapYear && (preMonth == 12) ? 29 : lastDayOfPreMonth;
        }
    }
}
