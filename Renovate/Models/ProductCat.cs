﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("ProductCats")]
    [Model(SingleName = "دسته کالا", MultipleName = "دسته های کالا")]
    public class ProductCat : BaseModel
    {
        private short productCatId;
        private short? parentProductCatId;
        private string title;
        private string comment = "";

        [DataMember]
        [Key]
        public short ProductCatId
        {
            get { return productCatId; }
            set { this.RaiseAndSetIfChanged(ref productCatId, value); }
        }

        [DataMember]
        public short? ParentProductCatId
        {
            get { return parentProductCatId; }
            set { this.RaiseAndSetIfChanged(ref parentProductCatId, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "عنوان دسته محصول نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged(ref title, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        [NotMapped]
        public List<ProductCat> Children { get; set; } = new List<ProductCat>();

        public ProductCat()
        {
            this.ValidationRule(vm => vm.Title, title => !title.IsNullOrWhiteSpace(), "عنوان نمی تواند خالی باشد");
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
