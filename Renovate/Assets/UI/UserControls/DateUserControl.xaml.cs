﻿using Renovate.Assets.DTOs;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Renovate.Assets.UI.UserControls
{
    /// <summary>
    /// Interaction logic for DateUserControl.xaml
    /// </summary>
    public partial class DateUserControl : UserControl
    {
        public DateUserControl()
        {
            InitializeComponent();

            DateDto.PropertyChanged += (s, e) =>
            {
                var forValidationProperties = new string[] { nameof(DateDto.Year), nameof(DateDto.Month), nameof(DateDto.Day) };
                if (forValidationProperties.Contains(e.PropertyName))
                {
                    if (e.PropertyName != nameof(DateDto.Day))
                    {
                        DateDto.CalendarDisplayDate = $"{((PersianMonths)DateDto.Month).GetAttribute<DisplayAttribute>().Name} {DateDto.Year:0000}";
                        CreateMonth();
                    }

                    LocateCurrentDate();
                    RefreshDate(ChangedDateTypes.UI);
                }
            };
            
            DateDto.Month = DateDto.Month; // Intentionally for CreateMonth() and LocateCurrentDate()
            
            this.LostFocus += (s, e) => { DateSelectionPopup.StaysOpen = false; };
            this.GotFocus += (s, e) => { DateSelectionPopup.StaysOpen = true; };
            NextButton.Click += (s, e) => { UpdateYearAndMonth(+1); };
            PreButton.Click += (s, e) => { UpdateYearAndMonth(-1); };
            DateTextBox.MouseDoubleClick += (s, e) => { DateDto.StringDate = DateTime.Today.ToPersian<CalendarDateTimeDto>().ToString(); };
        }

        public enum ChangedDateTypes { DateTime, UI }
        public EventHandler DateChanged;
        public bool IsReaction { get; set; }

        public DateTime Date
        {
            get { return (DateTime)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }
        public static readonly DependencyProperty DateProperty = DependencyProperty.Register("Date", typeof(DateTime), typeof(DateUserControl), new FrameworkPropertyMetadata(DateTime.Today, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnDateChanged));

        public string Title
        {
            get { return (string)base.GetValue(TitleProperty); }
            set { base.SetValue(TitleProperty, value); }
        }
        public static readonly DependencyProperty TitleProperty = DependencyProperty.Register("Title", typeof(string), typeof(DateUserControl), new PropertyMetadata(""));

        public CalendarDateTimeDto DateDto { get; set; } = DateTime.Today.ToPersian<CalendarDateTimeDto>();

        private static void OnDateChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var userControl = (DateUserControl)d;
            userControl.RefreshDate(ChangedDateTypes.DateTime);
            userControl.DateChanged?.Invoke(userControl, null);
        }

        void FillDateTimeDto(CalendarDateTimeDto input)
        {
            DateDto.Year = input.Year;
            DateDto.Month = input.Month;
            DateDto.Day = input.Day;
            DateDto.DayOfWeek = input.DayOfWeek;
        }

        public void RefreshDate(ChangedDateTypes changedDateType)
        {
            if (IsReaction)
                return;

            IsReaction = true;
            switch (changedDateType)
            {
                case ChangedDateTypes.DateTime:
                    FillDateTimeDto(Date.ToPersian<CalendarDateTimeDto>());
                    break;
                case ChangedDateTypes.UI:
                    SetCurrentValue(DateProperty, DateDto.ValidDate);
                    break;
                default:
                    break;
            }

            IsReaction = false;
        }

        private void SwitchButton_Click(object sender, RoutedEventArgs e)
        {
            DateDto.IsInYearMode = !DateDto.IsInYearMode;
        }

        private void DayButton_Click(object sender, RoutedEventArgs e)
        {
            var dayButton = (Button)sender;
            DateDto.Day = int.Parse(dayButton.Content.ToString());

            DateSelectionPopup.IsOpen = false;
        }

        private void MonthButton_DblClick(object sender, MouseButtonEventArgs e)
        {
            var monthButton = (Button)sender;
            DateDto.Month = int.Parse(monthButton.Name.Split('_')[1]);

            SwitchButton_Click(null, null);
        }

        private void CreateMonth()
        {
            int i = 1;
            DateDto.FillMonthInfo(out int lastDayOfPreMonth, out short firstDayOfWeekForThisMonth, out int thisMonthDaysCount);

            for (; i < firstDayOfWeekForThisMonth; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i}");
                btn.IsEnabled = false;
                btn.Content = $"{lastDayOfPreMonth + 1 - firstDayOfWeekForThisMonth + i}";
            }

            for (; i < firstDayOfWeekForThisMonth + thisMonthDaysCount; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i}");
                btn.IsEnabled = true;
                btn.Content = $"{i - firstDayOfWeekForThisMonth + 1}";
            }

            for (; i <= 42; i++)
            {
                var btn = (Button)DaysGrid.FindName($"D_{i}");
                btn.IsEnabled = false;
                btn.Content = $"{i + 1 - (firstDayOfWeekForThisMonth + thisMonthDaysCount)}";
            }
        }

        void LocateCurrentDate()
        {
            var dayToBeHighlighted = DaysGrid.Children.OfType<Button>().FirstOrDefault(b => b.IsEnabled && b.Content.ToString() == DateDto.Day.ToString());
            if (dayToBeHighlighted != null)
            {
                SelectedDayIndicatorRectangle.Visibility = Visibility.Visible;
                SelectedDayIndicatorRectangle.SetValue(Grid.RowProperty, dayToBeHighlighted.GetValue(Grid.RowProperty));
                SelectedDayIndicatorRectangle.SetValue(Grid.ColumnProperty, dayToBeHighlighted.GetValue(Grid.ColumnProperty));
            }
        }

        private void UpdateYearAndMonth(int increment)
        {
            if (DateDto.IsInYearMode)
                DateDto.Year += increment;
            else
            {
                var month = DateDto.Month + increment;
                if (month == 0 || month == 13)
                {
                    DateDto.Year += increment;
                    month = increment == 1 ? 1 : 12;
                }
                DateDto.Month = month;
            }
        }

        private void ToggleVisibility(object sender, RoutedEventArgs e)
        {
            DateSelectionPopup.IsOpen = !DateSelectionPopup.IsOpen;
        }
    }
}
