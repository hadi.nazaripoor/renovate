﻿using ReactiveUI;
using Renovate.Assets.Attributes;
using Renovate.Assets.DTOs;

namespace Renovate.Models.SearchModels
{
	public class PersonSearchModel : SearchBaseDto
    {
		private string fullName;
		private string callNumbers;
		private string nationalCodeOrEconomicCode;

        [Search(Title = "نام")]
		public string FullName
		{
			get { return fullName; }
			set { this.RaiseAndSetIfChanged(ref fullName, value); }
		}

		[Search(Title = "شماره های تماس")]
		public string CallNumbers
		{
			get { return callNumbers; }
			set { this.RaiseAndSetIfChanged(ref callNumbers, value); }
		}

		[Search(Title = "کد ملی یا کد اقتصادی")]
		public string NationalCodeOrEconomicCode
		{
			get { return nationalCodeOrEconomicCode; }
            set { this.RaiseAndSetIfChanged(ref nationalCodeOrEconomicCode, value); }
		}
	}
}
