﻿using ReactiveUI;
using ReactiveUI.Validation.Extensions;
using Renovate.Assets.Attributes;
using Renovate.Assets.Extensions;
using Renovate.Assets.Values.Constants;
using Renovate.Assets.Values.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Renovate.Models
{
    [DataContract]
    [Table("Accounts")]
    [Model(SingleName = "حساب", MultipleName = "حساب ها")]
    public class Account : BaseModel
    {
        private short accountId;
        private AccountTypes accountType;
        private string title;
        private string accountNo = "";
        private string cardNo = "";
        private string comment = "";

        private long primaryBalance;
        private long currentBalance;

        private bool isActive;

        private bool isDeleted;

        [DataMember]
        [Key]
        public short AccountId
        {
            get { return accountId; }
            set { this.RaiseAndSetIfChanged(ref accountId, value); }
        }

        [DataMember]
        [Required(ErrorMessage = "عنوان حساب نمی تواند خالی باشد")]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string Title
        {
            get { return title; }
            set { this.RaiseAndSetIfChanged(ref title, value); }
        }

        [DataMember]
        [Required]
        public AccountTypes AccountType
        {
            get { return accountType; }
            set { this.RaiseAndSetIfChanged(ref accountType, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string AccountNo
        {
            get { return accountNo; }
            set { this.RaiseAndSetIfChanged(ref accountNo, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.MEDIUM_STRING)]
        public string CardNo
        {
            get { return cardNo; }
            set { this.RaiseAndSetIfChanged(ref cardNo, value); }
        }

        [DataMember]
        [Required(AllowEmptyStrings = true)]
        [MaxLength(LengthConstants.VERY_LARGE_STRING)]
        public string Comment
        {
            get { return comment; }
            set { this.RaiseAndSetIfChanged(ref comment, value); }
        }

        [DataMember]
        [Required]
        public long PrimaryBalance
        {
            get { return primaryBalance; }
            set { this.RaiseAndSetIfChanged(ref primaryBalance, value); }
        }

        [DataMember]
        [Required]
        public long CurrentBalance
        {
            get { return currentBalance; }
            set { this.RaiseAndSetIfChanged(ref currentBalance, value); }
        }

        [DataMember]
        [Required]
        public bool IsActive
        {
            get { return isActive; }
            set { this.RaiseAndSetIfChanged(ref isActive, value); }
        }

        [DataMember]
        [Required]
        public bool IsDeleted
        {
            get { return isDeleted; }
            set { this.RaiseAndSetIfChanged(ref isDeleted, value); }
        }

        public Account()
        {
            this.ValidationRule(vm => vm.Title, title=> !title.IsNullOrWhiteSpace(), "عنوان حساب نمی تواند خالی باشد");
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
