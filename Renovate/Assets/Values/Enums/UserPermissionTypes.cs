﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Enums
{
    public enum UserPermissionTypes
    {
        Product,
        Person,
        Buy,
        Sale,
        Account,
        AccountTransition,
        Payment,
        Recieve,
        Cheque,
        Loan,
        Outgo,
        Income,
        User
    }
}
