﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Renovate.Assets.Values.Constants
{
    public class ImageSourceConstants
    {
        public const string ALL_ITEMS_SELECTED = @"/Renovate;component/Resources/Images/CommonIcons/allSelection.png",
                            SOME_ITEMS_SELECTED = @"/Renovate;component/Resources/Images/CommonIcons/partialSelection.png",
                            NO_ITEM_SELECTED = @"/Renovate;component/Resources/Images/CommonIcons/noSelection.png";
    }
}
