﻿using ReactiveUI;
using Renovate.Assets.DTOs;
using Renovate.Assets.UI.BaseClasses;
using Renovate.Assets.UI.CustomEventArgs;
using Renovate.Assets.UI.UserControls;
using Renovate.ViewModels;
using Renovate.Views;
using Renovate.Views.CUs;
using Renovate.Views.Lists;
using System;
using System.Data;
using System.Linq;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace Renovate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : TabbedWindow
    {
        MainViewModel ViewModel = App.MainViewModel;
        public MainWindow()
        {
            InitializeComponent();

            Loaded += (s, e) =>
            {
                this.DataContext = ViewModel;

                TitleBar.MenuClicked += (ss, ee) => { ViewModel.IsMenuVisible = !ViewModel.IsLocked && !ViewModel.IsMenuVisible; };

                ViewModel.WhenAnyValue(vm => vm.IsMenuVisible).Subscribe(isMenuVisible =>
                {
                    if (isMenuVisible)
                        ViewModel.IsSearchVisible = false;
                    var storyboard = Resources[isMenuVisible ? "ShowMenuStoryboard" : "HideMenuStoryboard"] as Storyboard;
                    storyboard.Begin();
                });

                ViewModel.WhenAnyValue(vm => vm.IsSearchVisible).Subscribe(isSearchVisible =>
                {
                    if (isSearchVisible)
                        ViewModel.IsMenuVisible = false;
                    var storyboard = Resources[isSearchVisible ? "ShowSearchStoryboard" : "HideSearchStoryboard"] as Storyboard;
                    storyboard.Begin();
                });

                ViewModel.InitiateSearch.Subscribe(searchModel =>
                {
                    this.searchUserControl.Initialize(searchModel);
                });

                IDisposable disposable = null;
                ViewModel.WhenAnyValue(vm => vm.DialogDto).Where(dto => dto != null).Subscribe(dialogDto =>
                {
                    disposable?.Dispose();
                    disposable = Observable.FromEventPattern(dialogUserControl, nameof(dialogUserControl.Returned))
                    .Select(ea => ((DialogEventArgs)ea.EventArgs).DialogResult).Subscribe(dialogResult =>
                    {
                        ViewModel.DialogResult = dialogResult;
                    });

                    dialogUserControl.Initialize(dialogDto);
                });

                ViewModel.WhenAnyValue(vm => vm.CreateUpdatePage).SkipWhile(page => page == null).Subscribe(createUpdatePage =>
                {
                    CreateUpdateFrame.RemoveBackEntry();
                    if (createUpdatePage == null)
                    {
                        CreateUpdateFrame.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        createUpdatePage.FontFamily = this.FontFamily;
                        CreateUpdateFrame.Navigate(createUpdatePage);
                        CreateUpdateFrame.Visibility = Visibility.Visible;
                    }
                });

                Frame.Navigate(new NoteListPage() { FontFamily = this.FontFamily });
                CreateUpdateFrame.Navigate(new LoginPage() { FontFamily = this.FontFamily });
            };
            PreviewKeyDown += MainWindow_PreviewKeyDown;
        }

        private void MainWindow_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (!ViewModel.IsLocked)
            {
                var shortcutData = new ShortcutDto();
                shortcutData.ModifierKeys = Keyboard.Modifiers;
                shortcutData.Key = e.Key;

                if (CreateUpdateFrame.Content is ShortcutPage && e.Key != Key.Enter && e.Key != Key.Escape)
                {
                    var shortcutPage = CreateUpdateFrame.Content as ShortcutPage;
                    shortcutPage.ShortcutDto.ModifierKeys = Keyboard.Modifiers;
                    shortcutPage.ShortcutDto.Key = e.Key;
                }
                //else
                //{
                //    if (shortcutData.IsValidShortcut)
                //    {
                //        var pageOrOperationEnum = ViewModel.GetShortcutRelatedPageOrOperationEnum(shortcutData.ShortcutOutput);
                //        if (pageOrOperationEnum != PageOrOperationType.None)
                //        {
                //            var subMenuItem = new SubMenuItemUC();
                //            subMenuItem.PageOrOperationEnum = pageOrOperationEnum;

                //            SMIUC_Clicked(subMenuItem, null);
                //        }
                //    }
                //}
            }
        }

        private void SMIUC_Clicked(object sender, EventArgs e)
        {
            var pageSource = (sender as SubMenuItemUserControl).NavigationLink.NavigationPageSource;
            if (pageSource != null && pageSource.Contains("ListPage.xaml"))
            {
                var page = Application.LoadComponent(new Uri(pageSource, UriKind.RelativeOrAbsolute));
                (page as Page).FontFamily = this.FontFamily;
                Frame.Navigate(page);
            }
        }
    }
}
