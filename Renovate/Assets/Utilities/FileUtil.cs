﻿using Renovate.Assets.DTOs;
using Renovate.Assets.Values.Enums;
using Renovate.Assets.Values.Variables;
using System;
using System.IO;

namespace Renovate.Assets.Utilities
{
    public class FileUtil
    {
        public static OperationResultDto ReadFile(string filePath)
        {
            var resolvedFilePath = GetResolvedPath(filePath);

            if (File.Exists(resolvedFilePath))
                return new OperationResultDto { Message = File.ReadAllText(resolvedFilePath) };

            return new OperationResultDto { Message = "File not found.", Result = OperationResults.Failed };
        }

        public static OperationResultDto WriteFile(string filePath, string contentToWrite)
        {
            var resolvedFilePath = GetResolvedPath(filePath);

            StreamWriter streamWriter = null;

            try
            {
                var directoryPath = Path.GetDirectoryName(resolvedFilePath);
                if (!Directory.Exists(directoryPath))
                    Directory.CreateDirectory(directoryPath);

                streamWriter = File.AppendText(resolvedFilePath);
            }
            catch (Exception exception)
            {
                return new OperationResultDto { Message = exception.Message, Result = OperationResults.Failed };
            }

            streamWriter.WriteLine(contentToWrite);
            streamWriter.Close();

            return new OperationResultDto();
        }

        private static string GetResolvedPath(string filePath) => filePath.StartsWith("@") ? RuntimeVariables.ApplicationPath : "" + filePath;
    }
}
